﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Repository
{
    using System.Linq;

    /// <summary>
    /// The entity-independent Read operation is pulled out into this parent repository.
    /// </summary>
    /// <typeparam name="T">Generic T type.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Read operation.
        /// </summary>
        /// <returns>IQueryable T collection.</returns>
        IQueryable<T> Read();

        /// <summary>
        /// Gets one entry from the implementation's table.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Generic T entry.</returns>
        T GetOne(int id);
    }
}
