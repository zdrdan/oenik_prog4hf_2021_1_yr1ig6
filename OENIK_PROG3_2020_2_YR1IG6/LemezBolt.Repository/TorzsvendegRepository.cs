﻿// <copyright file="TorzsvendegRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Repository
{
    using System;
    using System.Linq;
    using LemezBolt.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Implements the CRUD operations of the Torzsvendegek table.
    /// </summary>
    public class TorzsvendegRepository : ParentRepository<Torzsvendeg>, ITorzsvendegRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TorzsvendegRepository"/> class.
        /// Konstruktor.
        /// </summary>
        /// <param name="context">Sends the Dbcontext type parameter further towards ParentRepository's contructor.</param>
        [CLSCompliant(false)]
        public TorzsvendegRepository(DbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Returns an entry based on the given key value.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Torzsvendeg elemet a táblából.</returns>
        public override Torzsvendeg GetOne(int id)
        {
            return this.Read().SingleOrDefault(x => x.TorzsvendegID == id);
        }

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's activity status.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void AktivUpdate(bool ertek, int id)
        {
            this.GetOne(id).Aktiv = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's favorite style.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void KedvencStilusUpdate(string ertek, int id)
        {
            this.GetOne(id).KedvencStilus = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's name.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void NevUpdate(string ertek, int id)
        {
            this.GetOne(id).Nev = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's birthday date.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void SzuletesnapUpdate(string ertek, int id)
        {
            this.GetOne(id).Szuletesnap = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Inserts a Torzsvendeg entry into the table.
        /// </summary>
        /// <param name="torzsvendeg">New Torzsvendeg as parameter.</param>
        public void TorzsvendegCreate(Torzsvendeg torzsvendeg)
        {
            (this.Context as LemezBoltContext).Torzsvendegek.Add(torzsvendeg);
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Deletes a Torzsvendeg entry from the table.
        /// </summary>
        /// <param name="id">Key.</param>
        public void TorzsvendegDelete(int id)
        {
            (this.Context as LemezBoltContext).Torzsvendegek.Remove(this.GetOne(id));
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's e-mail.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void TvEmailUpdate(string ertek, int id)
        {
            this.GetOne(id).TvEmail = ertek;
            this.Context.SaveChanges();
        }
    }
}
