﻿// <copyright file="LemezRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Repository
{
    using System;
    using System.Linq;
    using LemezBolt.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Implements the CRUD operations of the Lemezek table.
    /// </summary>
    public class LemezRepository : ParentRepository<Lemez>, ILemezRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LemezRepository"/> class.
        /// Konstruktor.
        /// </summary>
        /// <param name="context">Sends the Dbcontext type parameter further towards ParentRepository's contructor.</param>
        [CLSCompliant(false)]
        public LemezRepository(DbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Returns an entry based on the given key value.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Lemez entry from the table.</returns>
        public override Lemez GetOne(int id)
        {
            return this.Read().SingleOrDefault(x => x.LemezID == id);
        }

        /// <summary>
        /// Updating the Eloado value of a Lemez entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void EloadoUpdate(string ertek, int id)
        {
            this.GetOne(id).Eloado = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the KiadasEve value of a Lemez entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void KiadasEveUpdate(string ertek, int id)
        {
            this.GetOne(id).KiadasEve = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the LemezCim value of a Lemez entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void LemezCimUpdate(string ertek, int id)
        {
            this.GetOne(id).LemezCim = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Adding a new entry to Lemezek.
        /// </summary>
        /// <param name="lemez">New Lemez as parameter.</param>
        public void LemezCreate(Lemez lemez)
        {
            (this.Context as LemezBoltContext).Lemezek.Add(lemez);
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Deleting an entry from Lemezek.
        /// </summary>
        /// <param name="id">Key.</param>
        public void LemezDelete(int id)
        {
            (this.Context as LemezBoltContext).Lemezek.Remove(this.GetOne(id));
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the Stilus value of a Lemez entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void StilusUpdate(string ertek, int id)
        {
            this.GetOne(id).Stilus = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the Tipus value of a Lemez entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void TipusUpdate(string ertek, int id)
        {
            this.GetOne(id).Tipus = ertek;
            this.Context.SaveChanges();
        }
    }
}
