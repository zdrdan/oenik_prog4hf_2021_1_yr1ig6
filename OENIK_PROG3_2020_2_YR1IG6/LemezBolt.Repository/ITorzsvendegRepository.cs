﻿// <copyright file="ITorzsvendegRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Repository
{
    using LemezBolt.Data;

    /// <summary>
    /// Törzsvendég CRUD metódusok.
    /// </summary>
    public interface ITorzsvendegRepository : IRepository<Torzsvendeg>
    {
        /// <summary>
        /// Inserts a Torzsvendeg entry into the table.
        /// </summary>
        /// <param name="torzsvendeg">Új törzsvendég as parameter.</param>
        void TorzsvendegCreate(Torzsvendeg torzsvendeg);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's name.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void NevUpdate(string ertek, int id);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's e-mail.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void TvEmailUpdate(string ertek, int id);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's favorite style.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void KedvencStilusUpdate(string ertek, int id);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's birthday date.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void SzuletesnapUpdate(string ertek, int id);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's activity status.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void AktivUpdate(bool ertek, int id);

        /// <summary>
        /// Deletes a Torzsvendeg entry from the table.
        /// </summary>
        /// <param name="id">Key.</param>
        void TorzsvendegDelete(int id);
    }
}
