﻿// <copyright file="VasarlasRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Repository
{
    using System;
    using System.Linq;
    using LemezBolt.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Implements the CRUD operations of the Vasarlas table.
    /// </summary>ok
    public class VasarlasRepository : ParentRepository<Vasarlas>, IVasarlasRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VasarlasRepository"/> class.
        /// Konstruktor.
        /// </summary>
        /// <param name="context">Sends the Dbcontext type parameter further towards ParentRepository's contructor.</param>
        [CLSCompliant(false)]
        public VasarlasRepository(DbContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Returns an entry based on the given key value.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Vasarlas entry from the table.</returns>
        public override Vasarlas GetOne(int id)
        {
            return this.Read().SingleOrDefault(x => x.VasarlasID == id);
        }

        /// <summary>
        /// Updating the AFA value of a Vasarlas entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void AFAUpdate(double ertek, int id)
        {
            this.GetOne(id).AFA = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the price value of a Vasarlas entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void ArUpdate(int ertek, int id)
        {
            this.GetOne(id).Ar = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the LemezID value of a Vasarlas entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void LemezIDUpdate(int ertek, int id)
        {
            this.GetOne(id).LemezID = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the TotzsvendegID value of a Vasarlas entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void TorzsvendegIDUpdate(int ertek, int id)
        {
            this.GetOne(id).TorzsvendegID = ertek;
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Adding a new entry to Vasarlasok.
        /// </summary>
        /// <param name="vasarlas">New Vasarlas as parameter.</param>
        public void VasarlasCreate(Vasarlas vasarlas)
        {
            (this.Context as LemezBoltContext).Vasarlasok.Add(vasarlas);
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Deleting an entry from Vasarlasok.
        /// </summary>
        /// <param name="id">Key.</param>
        public void VasarlasDelete(int id)
        {
            (this.Context as LemezBoltContext).Vasarlasok.Remove(this.GetOne(id));
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Updating the date of a Vasarlas entry.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void VetelDatumUpdate(string ertek, int id)
        {
            this.GetOne(id).VetelDatum = ertek;
            this.Context.SaveChanges();
        }
    }
}
