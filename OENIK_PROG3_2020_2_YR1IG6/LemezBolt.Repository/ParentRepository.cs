﻿// <copyright file="ParentRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Repository
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Implementation of IRepository.
    /// </summary>
    /// <typeparam name="T">Generic type T.</typeparam>
    public abstract class ParentRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Declares the database.
        /// </summary>
        private DbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParentRepository{T}"/> class.
        /// </summary>
        /// <param name="context">Database as parameter.</param>
        [CLSCompliant(false)]
        protected ParentRepository(DbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets or sets context.
        /// </summary>
        [CLSCompliant(false)]
        protected DbContext Context { get => this.context; set => this.context = value; }

        /// <summary>
        /// Read operation.
        /// </summary>
        /// <returns>IQueryable T collection.</returns>
        public IQueryable<T> Read()
        {
            return this.context.Set<T>();
        }

        /// <summary>
        /// Gets one entry from the implementation's table.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Entry from the table.</returns>
        public abstract T GetOne(int id);
    }
}
