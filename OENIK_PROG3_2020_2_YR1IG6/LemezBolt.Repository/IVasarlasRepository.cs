﻿// <copyright file="IVasarlasRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using LemezBolt.Data;

    /// <summary>
    /// Vasarlas CRUD metódusok.
    /// </summary>
    public interface IVasarlasRepository : IRepository<Vasarlas>
    {
        /// <summary>
        /// Inserting a new Vasarlas into the table.
        /// </summary>
        /// <param name="vasarlas">Új vásárlás as parameter.</param>
        void VasarlasCreate(Vasarlas vasarlas);

        /// <summary>
        /// Egy vásárlás lemezazonosítójának felülírása.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void LemezIDUpdate(int ertek, int id);

        /// <summary>
        /// Egy vásárlás törzsvendégazonosítójának felülírása.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void TorzsvendegIDUpdate(int ertek, int id);

        /// <summary>
        /// Egy vásárlás árának felülírása.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void ArUpdate(int ertek, int id);

        /// <summary>
        /// Egy vásárlás dátumának felülírása.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void VetelDatumUpdate(string ertek, int id);

        /// <summary>
        /// Egy vásárlás árának ÁFA értékének felülírása.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void AFAUpdate(double ertek, int id);

        /// <summary>
        /// Egy vásárlás törlése a táblából.
        /// </summary>
        /// <param name="id">Key.</param>
        void VasarlasDelete(int id);
    }
}
