﻿// <copyright file="ILemezRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Repository
{
    using LemezBolt.Data;

    /// <summary>
    /// Lemez CRUD operations.
    /// </summary>
    public interface ILemezRepository : IRepository<Lemez>
    {
        /// <summary>
        /// Inserts a Lemez entry into the table.
        /// </summary>
        /// <param name="lemez">New lemez as parameter.</param>
        void LemezCreate(Lemez lemez);

        /// <summary>
        /// Updating the value of a Lemez entry's style.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void StilusUpdate(string ertek, int id);

        /// <summary>
        /// Updating the value of a Lemez entry's type.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void TipusUpdate(string ertek, int id);

        /// <summary>
        /// Updating the value of a Lemez entry's performer.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void EloadoUpdate(string ertek, int id);

        /// <summary>
        /// Updating the value of a Lemez entry's title.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void LemezCimUpdate(string ertek, int id);

        /// <summary>
        /// Updating the value of a Lemez entry's release year.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void KiadasEveUpdate(string ertek, int id);

        /// <summary>
        /// Deletes a Lemez entry from the table.
        /// </summary>
        /// <param name="id">Key.</param>
        void LemezDelete(int id);
    }
}
