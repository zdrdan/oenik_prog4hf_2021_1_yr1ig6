﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// MainVM class.
    /// </summary>
    [CLSCompliant(false)]
    public sealed class MainVM : ViewModelBase, IDisposable
    {
        private MainLogic logic;
        private LemezVM selectedLemez;
        private ObservableCollection<LemezVM> allLemez;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
        {
            this.logic = new MainLogic();
            this.LoadCmd = new RelayCommand(() => this.AllLemez = new ObservableCollection<LemezVM>(this.logic.ApiGetLemez()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelLemez(this.SelectedLemez));
            this.AddCmd = new RelayCommand(() => this.logic.EditLemez(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditLemez(this.SelectedLemez, this.EditorFunc));
        }

        /// <summary>
        /// Gets or sets SelectedLemez.
        /// </summary>
        public LemezVM SelectedLemez
        {
            get { return this.selectedLemez; }
            set { this.selectedLemez = value; }
        }

        /// <summary>
        /// Gets AllLemez.
        /// </summary>
        public ObservableCollection<LemezVM> AllLemez
        {
            get { return this.allLemez; }
            private set { this.Set(ref this.allLemez, value); }
        }

        /// <summary>
        /// Gets or sets EditorFunc.
        /// </summary>
        public Func<LemezVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets Addcmd.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets Delcmd.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets Modcmd.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets Loadcmd.
        /// </summary>
        public ICommand LoadCmd { get; private set; }

        /// <summary>
        /// Disposes of logic.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.logic.Dispose();
            return;
        }

        /// <summary>
        /// Sets allLemez.
        /// </summary>
        /// <param name="alllemez">ObservableCollectionLemezVM instance.</param>
        public void SetAllLemez(ObservableCollection<LemezVM> alllemez)
        {
            this.AllLemez = alllemez;
        }
    }
}
