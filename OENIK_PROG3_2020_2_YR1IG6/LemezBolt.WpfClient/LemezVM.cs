﻿// <copyright file="LemezVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// LemezVM class.
    /// </summary>
    [CLSCompliant(false)]
    public class LemezVM : ObservableObject
    {
        private int id;
        private string eloado;
        private string lemezCim;
        private string tipus;
        private string kiadasEve;
        private string stilus;

        /// <summary>
        /// Gets or sets LemezID.
        /// </summary>
        public int LemezID
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets eloado.
        /// </summary>
        public string Eloado
        {
            get { return this.eloado; }
            set { this.Set(ref this.eloado, value); }
        }

        /// <summary>
        /// Gets or sets LemezCim.
        /// </summary>
        public string LemezCim
        {
            get { return this.lemezCim; }
            set { this.Set(ref this.lemezCim, value); }
        }

        /// <summary>
        /// Gets or sets kiadaseve.
        /// </summary>
        public string KiadasEve
        {
            get { return this.kiadasEve; }
            set { this.Set(ref this.kiadasEve, value); }
        }

        /// <summary>
        /// Gets or sets tipus.
        /// </summary>
        public string Tipus
        {
            get { return this.tipus; }
            set { this.Set(ref this.tipus, value); }
        }

        /// <summary>
        /// Gets or sets stilus.
        /// </summary>
        public string Stilus
        {
            get { return this.stilus; }
            set { this.Set(ref this.stilus, value); }
        }

        /// <summary>
        /// Copies from another VM.
        /// </summary>
        /// <param name="other">LemezVM instance.</param>
        public void CopyFrom(LemezVM other)
        {
            if (other == null)
            {
                return;
            }

            this.LemezID = other.LemezID;
            this.Eloado = other.Eloado;
            this.LemezCim = other.LemezCim;
            this.Tipus = other.Tipus;
            this.KiadasEve = other.KiadasEve;
            this.Stilus = other.Stilus;
        }
    }
}
