﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// MainLogic class.
    /// </summary>
    public sealed class MainLogic : IDisposable
    {
        private string url = "http://localhost:63869/LemezApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);
        private Dictionary<string, string> postData;

        /// <summary>
        /// Gets lemez.
        /// </summary>
        /// <returns>List instance.</returns>
        [CLSCompliant(false)]
        public IEnumerable<LemezVM> ApiGetLemez()
        {
            string json = this.client.GetStringAsync(new Uri(this.url + "all")).Result;
            var list = JsonSerializer.Deserialize<List<LemezVM>>(json, this.jsonOptions);

            return list;
        }

        /// <summary>
        /// Deletes lemez.
        /// </summary>
        /// <param name="lemez">LemezVM instance.</param>
        [CLSCompliant(false)]
        public void ApiDelLemez(LemezVM lemez)
        {
            bool success = false;
            if (lemez != null)
            {
                string json = this.client.GetStringAsync(new Uri(this.url + "del/" + lemez.LemezID.ToString(CultureInfo.CurrentCulture))).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <summary>
        /// Disposes of the client.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.client.Dispose();
        }

        /// <summary>
        /// Edits lemez.
        /// </summary>
        /// <param name="lemez">LemezVM instance.</param>
        /// <param name="editorFunc">Function.</param>
        [CLSCompliant(false)]
        public void EditLemez(LemezVM lemez, Func<LemezVM, bool> editorFunc)
        {
            LemezVM clone = new LemezVM();
            if (lemez != null)
            {
                clone.CopyFrom(lemez);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (lemez != null)
                {
                    success = this.ApiEditLemez(clone, true);
                }
                else
                {
                    success = this.ApiEditLemez(clone, false);
                }
            }

            SendMessage(success == true);
        }

        private static void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "LemezResult");
        }

        private bool ApiEditLemez(LemezVM lemez, bool isEditing)
        {
            if (lemez == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");
            this.postData = new Dictionary<string, string>();
            if (isEditing)
            {
                this.postData.Add("LemezID", lemez.LemezID.ToString(CultureInfo.CurrentCulture));
            }

            this.postData.Add("Eloado", lemez.Eloado);
            this.postData.Add("Tipus", lemez.Tipus);
            this.postData.Add("Stilus", lemez.Stilus);
            this.postData.Add("LemezCim", lemez.LemezCim);
            this.postData.Add("KiadasEve", lemez.KiadasEve);

            FormUrlEncodedContent temp = new FormUrlEncodedContent(this.postData);
            string json = this.client.PostAsync(new Uri(myUrl), temp).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            temp.Dispose();
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}
