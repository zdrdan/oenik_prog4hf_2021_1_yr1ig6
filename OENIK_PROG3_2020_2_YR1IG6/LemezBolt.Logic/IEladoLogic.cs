﻿// <copyright file="IEladoLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic
{
    using System.Linq;
    using System.Threading.Tasks;
    using LemezBolt.Data;

    /// <summary>
    /// Operations of the seller.
    /// </summary>
    public interface IEladoLogic
    {
        //----------------------------------------------Vásárlás C R U---------------------------------------------------

        /// <summary>
        /// Inserts a Vasarlas entry into the table.
        /// </summary>
        /// <param name="vasarlas">New Vasarlas as parameter.</param>
        void VasarlasCreateLogic(Vasarlas vasarlas);

        /// <summary>
        /// Read operation.
        /// </summary>
        /// <returns>IQueriable Vasarlas collection.</returns>
        IQueryable<Vasarlas> VasarlasRead();

        /// <summary>
        /// Gets one entry from the Vasarlasok table.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Vasarlas entry.</returns>
        Vasarlas VasarlasGetOne(int id);

        /// <summary>
        /// Updating the value of a Vasarlas entry's LemezID.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void LemezIDUpdateLogic(int ertek, int id);

        /// <summary>
        /// Updating the value of a Vasarlas entry's TorzsvendegID.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void TorzsvendegIDUpdateLogic(int ertek, int id);

        /// <summary>
        /// Updating the value of a Vasarlas entry's price.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void ArUpdateLogic(int ertek, int id);

        /// <summary>
        /// Updating the value of a Vasarlas entry's date.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void VetelDatumUpdateLogic(string ertek, int id);

        /// <summary>
        /// Updating the value of a Vasarlas entry's ÁFA.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void AFAUpdateLogic(double ertek, int id);

        //---------------------------------------------Törzsvendég R U--------------------------------------------------

        /// <summary>
        /// Read operation.
        /// </summary>
        /// <returns>IQueriable Vasarlas elem.</returns>
        IQueryable<Torzsvendeg> TorzsvendegRead();

        /// <summary>
        /// Gets one entry from the Torzsvendegek table.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Torzsvendeg entry.</returns>
        Torzsvendeg TorzsvendegGetOne(int id);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's name.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void NevUpdateLogic(string ertek, int id);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's e-mail.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void TvEmailUpdateLogic(string ertek, int id);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's favorite style.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void KedvencStilusUpdateLogic(string ertek, int id);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's birthday date.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void SzuletesnapUpdateLogic(string ertek, int id);

        //---------------------------------------------NONCRUD----------------------------------------------------

        /// <summary>
        /// Gets the customers and the amount of cds they bought.
        /// </summary>
        /// <returns>VeteletiResults entries, containing Torzsvendeg instances and their bought cd counts.</returns>
        IQueryable<VeteleiResult> TorzsvendegVetelei();

        /// <summary>
        /// Ques the NON-CRUD operation.
        /// </summary>
        /// <returns>VeteleiResult entries.</returns>
        Task<IQueryable<VeteleiResult>> TorzsvendegVeteleiAsync();
    }
}
