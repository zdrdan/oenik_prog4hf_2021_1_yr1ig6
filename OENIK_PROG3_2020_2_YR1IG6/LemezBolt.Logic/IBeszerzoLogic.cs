﻿// <copyright file="IBeszerzoLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic
{
    using System.Linq;
    using LemezBolt.Data;

    /// <summary>
    /// Operations of the buyer.
    /// </summary>
    public interface IBeszerzoLogic
    {
        //---------------------------------------Lemez C R U--------------------------------------------

        /// <summary>
        /// Inserts a Lemez entry to the table.
        /// </summary>
        /// <param name="lemez">New Lemez as parameter.</param>
        void LemezCreateLogic(Lemez lemez);

        /// <summary>
        /// Read operation.
        /// </summary>
        /// <returns>IQueriable Lemez collection.</returns>
        IQueryable<Lemez> LemezRead();

        /// <summary>
        /// Gets one entry from the Lemezek table.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Lemez entry.</returns>
        Lemez LemezGetOne(int id);

        /// <summary>
        /// Updating the value of a Lemez entry's style.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void StilusUpdateLogic(string ertek, int id);

        /// <summary>
        /// Updating the value of a Lemez entry's type.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void TipusUpdateLogic(string ertek, int id);

        /// <summary>
        /// Updating the value of a Lemez entry's performer.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void EloadoUpdateLogic(string ertek, int id);

        /// <summary>
        /// Updating the value of a Lemez entry's title.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void LemezCimUpdateLogic(string ertek, int id);

        /// <summary>
        /// Updating the value of a Lemez entry's release year.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void KiadasEveUpdateLogic(string ertek, int id);
    }
}