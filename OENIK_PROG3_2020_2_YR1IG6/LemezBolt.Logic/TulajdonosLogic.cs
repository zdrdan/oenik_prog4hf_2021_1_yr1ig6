﻿// <copyright file="TulajdonosLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using LemezBolt.Data;
    using LemezBolt.Repository;

    /// <summary>
    /// Implementation of the owner's operations' logic.
    /// </summary>
    public class TulajdonosLogic : ITulajdonosLogic
    {
        private readonly ILemezRepository lemezRepository;
        private readonly ITorzsvendegRepository torzsvendegRepository;
        private readonly IVasarlasRepository vasarlasRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TulajdonosLogic"/> class.
        /// </summary>
        /// <param name="lemezRepository">Lemez repository as parameter.</param>
        /// <param name="torzsvendegRepository">Torzsvendeg repository as parameter.</param>
        /// <param name="vasarlasRepository">Vasarlas repository as parameter.</param>
        public TulajdonosLogic(ILemezRepository lemezRepository, ITorzsvendegRepository torzsvendegRepository, IVasarlasRepository vasarlasRepository)
        {
            this.lemezRepository = lemezRepository;
            this.torzsvendegRepository = torzsvendegRepository;
            this.vasarlasRepository = vasarlasRepository;
        }

        //-------------------------------Lemez D--------------------------------------

        /// <summary>
        /// Calling LemezRepository's LemezDelete implementation.
        /// </summary>
        /// <param name="id">Key.</param>
        public void LemezDeleteLogic(int id)
        {
            this.lemezRepository.LemezDelete(id);
        }

        //--------------------------------Torzsvendeg C U D-------------------------------

        /// <summary>
        /// Calling TorzsvendegRepository's TorzsvendegCreate implementation.
        /// </summary>
        /// <param name="torzsvendeg">New Torzsvendeg as parameter.</param>
        public void TorzsvendegCreateLogic(Torzsvendeg torzsvendeg)
        {
            this.torzsvendegRepository.TorzsvendegCreate(torzsvendeg);
        }

        /// <summary>
        /// Calling TorzsvendegRepository's AktivUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void AktivUpdateLogic(bool ertek, int id)
        {
            this.torzsvendegRepository.AktivUpdate(ertek, id);
        }

        /// <summary>
        /// Calling TorzsvendegRepository's TorzsvendegDelete implementation.
        /// </summary>
        /// <param name="id">Key.</param>
        public void TorzsvendegDeleteLogic(int id)
        {
            this.torzsvendegRepository.TorzsvendegDelete(id);
        }

        //-------------------------------------------Vasarlas D----------------------------------------------

        /// <summary>
        /// Calling VasarlasRepository's VasarlasDelete implementation.
        /// </summary>
        /// <param name="id">Key.</param>
        public void VasarlasDeleteLogic(int id)
        {
            this.vasarlasRepository.VasarlasDelete(id);
        }

        //---------------------------------------------NONCRUD----------------------------------------------------

        /// <summary>
        /// Which Lemez entry was bought the most times.
        /// </summary>
        /// <returns>Lemez entry.</returns>
        public IQueryable<Lemez> LegnepszerubbLemez()
        {
            var lemezek = this.lemezRepository.Read();
            int legtobbDb = (from x in this.lemezRepository.Read()
                             join y in this.vasarlasRepository.Read() on x.LemezID equals y.LemezID
                             group x by x.LemezID into g
                             orderby g.Count() descending
                             select g.Count()).FirstOrDefault();
            var idLista = from x in this.lemezRepository.Read()
                          join y in this.vasarlasRepository.Read() on x.LemezID equals y.LemezID
                          group x by x.LemezID into g
                          where g.Count() == legtobbDb
                          select g.Key;
            return lemezek.Where(x => idLista.Contains(x.LemezID));
        }

        /// <summary>
        /// Ques the NON-CRUD operation.
        /// </summary>
        /// <returns>Lemez entries.</returns>
        public Task<IQueryable<Lemez>> LegnepszerubbLemezAsync()
        {
            return Task.Run(() => this.LegnepszerubbLemez());
        }

        /// <summary>
        /// Which customers bought from this performer.
        /// </summary>
        /// <param name="eloado">Name of the performer.</param>
        /// <returns>Torzsvendeg entries.</returns>
        public IQueryable<Torzsvendeg> VasarlokNevei(string eloado)
        {
            var vendegek = (from x in this.lemezRepository.Read()
                            join y in this.vasarlasRepository.Read() on x.LemezID equals y.LemezID
                            join z in this.torzsvendegRepository.Read() on y.TorzsvendegID equals z.TorzsvendegID
                            where x.Eloado == eloado
                            select z).Distinct();
            return vendegek;
        }

        /// <summary>
        /// Ques the NON-CRUD operation.
        /// </summary>
        /// <param name="eloado">Name of the performer.</param>
        /// <returns>Torzsvendeg entries.</returns>
        public Task<IQueryable<Torzsvendeg>> VasarlokNeveiAsync(string eloado)
        {
            return Task.Run(() => this.VasarlokNevei(eloado));
        }
    }
}