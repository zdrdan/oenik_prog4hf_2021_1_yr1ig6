﻿// <copyright file="VeteleiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using LemezBolt.Data;

    /// <summary>
    /// A type for the results of the TorzsvendegVetelei NON-CRUD method.
    /// </summary>
    public class VeteleiResult
    {
        private string tvendeg;
        private int db;

        /// <summary>
        /// Initializes a new instance of the <see cref="VeteleiResult"/> class.
        /// </summary>
        /// <param name="tvendeg">Torzsvendeg instance.</param>
        /// <param name="db">int variable.</param>
        public VeteleiResult(string tvendeg, int db)
        {
            this.tvendeg = tvendeg;
            this.db = db;
        }

        /// <summary>
        /// Gets or sets Db.
        /// </summary>
        public int Db { get => this.db; set => this.db = value; }

        /// <summary>
        /// Gets or sets Tvendeg.
        /// </summary>
        public string Tvendeg { get => this.tvendeg; set => this.tvendeg = value; }

        /// <summary>
        /// Returns with true, if the object in the parameter is exactly like the one calling this method.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>A boolean value.</returns>
        public override bool Equals(object obj)
        {
            if (obj is VeteleiResult)
            {
                VeteleiResult other = obj as VeteleiResult;
                return this.Tvendeg == other.Tvendeg &&
                       this.db == other.Db;
            }

            return false;
        }

        /// <summary>
        /// A simple implementation of GetHashCode.
        /// </summary>
        /// <returns>Integer.</returns>
        public override int GetHashCode()
        {
            return this.Db;
        }
    }
}
