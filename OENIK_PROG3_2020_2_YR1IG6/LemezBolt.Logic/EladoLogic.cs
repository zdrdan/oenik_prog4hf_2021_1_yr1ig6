﻿// <copyright file="EladoLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic
{
    using System.Linq;
    using System.Threading.Tasks;
    using LemezBolt.Data;
    using LemezBolt.Repository;

    /// <summary>
    /// Implementation of the seller's operations' logic.
    /// </summary>
    public class EladoLogic : IEladoLogic
    {
        private readonly IVasarlasRepository vasarlasRepository;
        private readonly ITorzsvendegRepository torzsvendegRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="EladoLogic"/> class.
        /// </summary>
        /// <param name="vasarlasRepository">Vasarlas repository as parameter.</param>
        /// <param name="torzsvendegRepository">Torzsvendeg repository as parameter.</param>
        public EladoLogic(IVasarlasRepository vasarlasRepository, ITorzsvendegRepository torzsvendegRepository)
        {
            this.vasarlasRepository = vasarlasRepository;
            this.torzsvendegRepository = torzsvendegRepository;
        }

        //----------------------------------Vasarlas C R U-------------------------------

        /// <summary>
        /// Gets one entry from the Vasarlasok table.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Vasarlas entry.</returns>
        public Vasarlas VasarlasGetOne(int id)
        {
            return this.vasarlasRepository.GetOne(id);
        }

        /// <summary>
        /// Calling VasarlasRepository's VasarlasCreate implementation.
        /// </summary>
        /// <param name="vasarlas">New Vasarlass as parameter.</param>
        public void VasarlasCreateLogic(Vasarlas vasarlas)
        {
            this.vasarlasRepository.VasarlasCreate(vasarlas);
        }

        /// <summary>
        /// Calling VasarlasRepository's Read implementation.
        /// </summary>
        /// <returns>An ILemezRepository DbSet.</returns>
        public IQueryable<Vasarlas> VasarlasRead()
        {
            return this.vasarlasRepository.Read();
        }

        /// <summary>
        /// Calling VasarlasRepository's AFAUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void AFAUpdateLogic(double ertek, int id)
        {
            this.vasarlasRepository.AFAUpdate(ertek, id);
        }

        /// <summary>
        /// Calling VasarlasRepository's ArUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void ArUpdateLogic(int ertek, int id)
        {
            this.vasarlasRepository.ArUpdate(ertek, id);
        }

        /// <summary>
        /// Calling VasarlasRepository's LemezIDUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void LemezIDUpdateLogic(int ertek, int id)
        {
            this.vasarlasRepository.LemezIDUpdate(ertek, id);
        }

        /// <summary>
        /// Calling VasarlasRepository's TorzsvendegIDUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void TorzsvendegIDUpdateLogic(int ertek, int id)
        {
            this.vasarlasRepository.TorzsvendegIDUpdate(ertek, id);
        }

        /// <summary>
        /// Calling VasarlasRepository's VetelDatumUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void VetelDatumUpdateLogic(string ertek, int id)
        {
            this.vasarlasRepository.VetelDatumUpdate(ertek, id);
        }

        //--------------------------------Torzsvendeg R U-------------------------------

        /// <summary>
        /// Calling TorzsvendegRepository's KedvencStilusUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void KedvencStilusUpdateLogic(string ertek, int id)
        {
            this.torzsvendegRepository.KedvencStilusUpdate(ertek, id);
        }

        /// <summary>
        /// Calling TorzsvendegRepository's NevUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void NevUpdateLogic(string ertek, int id)
        {
            this.torzsvendegRepository.NevUpdate(ertek, id);
        }

        /// <summary>
        /// Calling TorzsvendegRepository's SzuletesnapUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void SzuletesnapUpdateLogic(string ertek, int id)
        {
            this.torzsvendegRepository.SzuletesnapUpdate(ertek, id);
        }

        /// <summary>
        /// Calling TorzsvendegRepository's Read implementation.
        /// </summary>
        /// <returns>Egy ITorzsvendegRepository DbSetet ad vissza.</returns>
        public IQueryable<Torzsvendeg> TorzsvendegRead()
        {
            return this.torzsvendegRepository.Read();
        }

        /// <summary>
        /// Gets one entry from the Torzsvendegek table.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Torzsvendeg entry.</returns>
        public Torzsvendeg TorzsvendegGetOne(int id)
        {
            return this.torzsvendegRepository.GetOne(id);
        }

        /// <summary>
        /// Calling TorzsvendegRepository's TvEmailUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void TvEmailUpdateLogic(string ertek, int id)
        {
            this.torzsvendegRepository.TvEmailUpdate(ertek, id);
        }

        //---------------------------------------------NONCRUD----------------------------------------------------

        /// <summary>
        /// Gets the customers and the amount of cds they bought.
        /// </summary>
        /// <returns>VeteleiResult entries.</returns>
        public IQueryable<VeteleiResult> TorzsvendegVetelei()
        {
            var a = from x in this.torzsvendegRepository.Read()
                    join y in this.vasarlasRepository.Read() on x.TorzsvendegID equals y.TorzsvendegID
                    group x by x.TorzsvendegID into g
                    select new { id = g.Key, db = g.Count() };
            var b = from x in a
                    join y in this.torzsvendegRepository.Read() on x.id equals y.TorzsvendegID
                    select new VeteleiResult(y.Nev, x.db);
            return b;
        }

        /// <summary>
        /// Ques the NON-CRUD operation.
        /// </summary>
        /// <returns>VeteleiResult entries.</returns>
        public Task<IQueryable<VeteleiResult>> TorzsvendegVeteleiAsync()
        {
            return Task.Run(() => this.TorzsvendegVetelei());
        }
    }
}
