﻿// <copyright file="BeszerzoLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic
{
    using System.Linq;
    using LemezBolt.Data;
    using LemezBolt.Repository;

    /// <summary>
    /// Implementation of the buyer's operations' logic.
    /// </summary>
    public class BeszerzoLogic : IBeszerzoLogic
    {
        private readonly ILemezRepository lemezRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="BeszerzoLogic"/> class.
        /// </summary>
        /// <param name="lemezRepository">Lemez repository as parameter.</param>
        public BeszerzoLogic(ILemezRepository lemezRepository)
        {
            this.lemezRepository = lemezRepository;
        }

        //-------------------------------Lemez C R U---------------------------------

        /// <summary>
        /// Calling LemezRepository's EloadoUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void EloadoUpdateLogic(string ertek, int id)
        {
            this.lemezRepository.EloadoUpdate(ertek, id);
        }

        /// <summary>
        /// Calling LemezRepository's KiadasEveUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void KiadasEveUpdateLogic(string ertek, int id)
        {
            this.lemezRepository.KiadasEveUpdate(ertek, id);
        }

        /// <summary>
        /// Calling LemezRepository's LemezCimUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void LemezCimUpdateLogic(string ertek, int id)
        {
            this.lemezRepository.LemezCimUpdate(ertek, id);
        }

        /// <summary>
        /// Calling LemezRepository's LemezCreate implementation.
        /// </summary>
        /// <param name="lemez">New Lemez as parameter.</param>
        public void LemezCreateLogic(Lemez lemez)
        {
            this.lemezRepository.LemezCreate(lemez);
        }

        /// <summary>
        /// Calling LemezRepository's Read implementation.
        /// </summary>
        /// <returns>An ILemezRepository DbSet.</returns>
        public IQueryable<Lemez> LemezRead()
        {
            return this.lemezRepository.Read();
        }

        /// <summary>
        /// Gets one entry from the Lemezek table.
        /// </summary>
        /// <param name="id">Key.</param>
        /// <returns>Lemez entry.</returns>
        public Lemez LemezGetOne(int id)
        {
            return this.lemezRepository.GetOne(id);
        }

        /// <summary>
        /// Calling LemezRepository's StilusUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void StilusUpdateLogic(string ertek, int id)
        {
            this.lemezRepository.StilusUpdate(ertek, id);
        }

        /// <summary>
        /// Calling LemezRepository's TipusUpdate implementation.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        public void TipusUpdateLogic(string ertek, int id)
        {
            this.lemezRepository.TipusUpdate(ertek, id);
        }
    }
}
