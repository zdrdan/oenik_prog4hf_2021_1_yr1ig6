﻿// <copyright file="ITulajdonosLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic
{
    using System.Linq;
    using System.Threading.Tasks;
    using LemezBolt.Data;

    /// <summary>
    /// Operations of the owner.
    /// </summary>
    public interface ITulajdonosLogic
    {
        //-----------------------------------------------------Lemez D-----------------------------------------------------------

        /// <summary>
        /// Deletes a Lemez entry from the table.
        /// </summary>
        /// <param name="id">Key.</param>
        void LemezDeleteLogic(int id);

        //----------------------------------------------------Törzsvendég C U D------------------------------------------------------

        /// <summary>
        /// Inserts a Torzsvendeg entry into the table.
        /// </summary>
        /// <param name="torzsvendeg">New törzsvendég as parameter.</param>
        void TorzsvendegCreateLogic(Torzsvendeg torzsvendeg);

        /// <summary>
        /// Updating the value of a Torzsvendeg entry's activity status.
        /// </summary>
        /// <param name="ertek">New value.</param>
        /// <param name="id">Key.</param>
        void AktivUpdateLogic(bool ertek, int id);

        /// <summary>
        /// Deletes a Torzsvendeg entry from the table.
        /// </summary>
        /// <param name="id">Key.</param>
        void TorzsvendegDeleteLogic(int id);

        //----------------------------------------------------Vásárlás D---------------------------------------------------------

        /// <summary>
        /// Deletes a Vasarlas entry from the table.
        /// </summary>
        /// <param name="id">Key.</param>
        void VasarlasDeleteLogic(int id);

        //------------------------------------------------------NONCRUD-----------------------------------------------------------

        /// <summary>
        /// Gets the most bought Lemez entry/entries.
        /// </summary>
        /// <returns>Lemez entries.</returns>
        IQueryable<Lemez> LegnepszerubbLemez();

        /// <summary>
        /// Ques the NON-CRUD operation.
        /// </summary>
        /// <returns>Lemez entry.</returns>
        Task<IQueryable<Lemez>> LegnepszerubbLemezAsync();

        /// <summary>
        /// Gets the customer entries, who have bought from this performer.
        /// </summary>
        /// <param name="eloado">Performer's name.</param>
        /// <returns>Torzsvendeg entries.</returns>
        IQueryable<Torzsvendeg> VasarlokNevei(string eloado);

        /// <summary>
        /// Ques the NON-CRUD operation.
        /// </summary>
        /// <param name="eloado">Name of the performer.</param>
        /// <returns>Torzsvendeg entries.</returns>
        Task<IQueryable<Torzsvendeg>> VasarlokNeveiAsync(string eloado);
    }
}