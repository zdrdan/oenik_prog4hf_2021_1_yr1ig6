﻿// <copyright file="EladoLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LemezBolt.Data;
    using LemezBolt.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for testing the methods in EladoLogic.
    /// </summary>
    [TestFixture]
    internal static class EladoLogicTests
    {
        /// <summary>
        /// Tests the VasarlasRead operation.
        /// </summary>
        [Test]
        public static void TestVasarlasRead()
        {
            // Arrange
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            List<Vasarlas> list = new List<Vasarlas>()
            {
                new Vasarlas() { VasarlasID = 000001, Ar = 5880, VetelDatum = "2019.01.05.", AFA = 5880 * 0.25 },
                new Vasarlas() { VasarlasID = 000002, Ar = 3490, VetelDatum = "2019.01.06.", AFA = 3490 * 0.25 },
                new Vasarlas() { VasarlasID = 000003, Ar = 2970, VetelDatum = "2019.01.06.", AFA = 2970 * 0.25 },
                new Vasarlas() { VasarlasID = 000004, Ar = 3490, VetelDatum = "2019.01.07.", AFA = 3490 * 0.25 },
                new Vasarlas() { VasarlasID = 000005, Ar = 7390, VetelDatum = "2019.01.07.", AFA = 7390 * 0.25 },
            };
            list[0].LemezID = 00001;
            list[1].LemezID = 00014;
            list[2].LemezID = 00015;
            list[3].LemezID = 00014;
            list[4].LemezID = 00006;
            list[0].TorzsvendegID = 001;
            list[1].TorzsvendegID = 002;
            list[2].TorzsvendegID = 002;
            list[3].TorzsvendegID = 004;
            list[4].TorzsvendegID = 004;
            mockedVasarlasRepo.Setup(repo => repo.Read()).Returns(list.AsQueryable());
            EladoLogic eLogic = new EladoLogic(mockedVasarlasRepo.Object, mockedTorzsvendegRepo.Object);

            // Act
            List<Vasarlas> result = eLogic.VasarlasRead().ToList();

            // Assert
            mockedVasarlasRepo.Verify(repo => repo.Read(), Times.Once);
        }

        /// <summary>
        /// Tests the TorzsvendegRead operation.
        /// </summary>
        [Test]
        public static void TestTorzsvendegRead()
        {
            // Arrange
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            List<Torzsvendeg> list = new List<Torzsvendeg>()
            {
                new Torzsvendeg() { TorzsvendegID = 001, Nev = "Horváth Péter", TvEmail = "hpeter@gmail.com", KedvencStilus = "Rock", Szuletesnap = "06.05.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 002, Nev = "Tóth Krisztina", TvEmail = "tkriszt@gmail.com", KedvencStilus = "RnB", Szuletesnap = "09.01.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 003, Nev = "Kovács Simon", TvEmail = "kosi@freemail.hu", KedvencStilus = "RnB", Szuletesnap = "11.03.", Aktiv = false },
                new Torzsvendeg() { TorzsvendegID = 004, Nev = "Róbert Bence", TvEmail = "robertbeni@gmail.com", KedvencStilus = "Electronic", Szuletesnap = "01.20.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 005, Nev = "Horváth Tamás", TvEmail = "htamas@gmail.com", KedvencStilus = "Jazz", Szuletesnap = "04.05.", Aktiv = true },
            };
            mockedTorzsvendegRepo.Setup(repo => repo.Read()).Returns(list.AsQueryable());
            EladoLogic eLogic = new EladoLogic(mockedVasarlasRepo.Object, mockedTorzsvendegRepo.Object);

            // Act
            List<Torzsvendeg> result = eLogic.TorzsvendegRead().ToList();

            // Assert
            mockedTorzsvendegRepo.Verify(repo => repo.Read(), Times.Once);
        }

        /// <summary>
        /// Tests the VasarlasCreateLogic operation.
        /// </summary>
        [Test]
        public static void TestVasarlasCreateLogic()
        {
            // Arrange
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            List<Vasarlas> list = new List<Vasarlas>();
            Vasarlas receipt = new Vasarlas() { VasarlasID = 000001, Ar = 5880, VetelDatum = "2019.01.05.", AFA = 5880 * 0.25 };
            mockedVasarlasRepo.Setup(repo => repo.VasarlasCreate(It.IsAny<Vasarlas>())).Callback<Vasarlas>(entry => list.Add(entry));
            EladoLogic eLogic = new EladoLogic(mockedVasarlasRepo.Object, mockedTorzsvendegRepo.Object);

            // Act
            eLogic.VasarlasCreateLogic(receipt);

            // Arrange
            mockedVasarlasRepo.Verify(repo => repo.VasarlasCreate(receipt), Times.Once);
        }

        /// <summary>
        /// Tests the TorzsvendegGetOne operation.
        /// </summary>
        [Test]
        public static void TestTorzsvendegGetOne()
        {
            // Arrange
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            List<Torzsvendeg> list = new List<Torzsvendeg>()
            {
                new Torzsvendeg() { TorzsvendegID = 001, Nev = "Horváth Péter", TvEmail = "hpeter@gmail.com", KedvencStilus = "Rock", Szuletesnap = "06.05.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 002, Nev = "Tóth Krisztina", TvEmail = "tkriszt@gmail.com", KedvencStilus = "RnB", Szuletesnap = "09.01.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 003, Nev = "Kovács Simon", TvEmail = "kosi@freemail.hu", KedvencStilus = "RnB", Szuletesnap = "11.03.", Aktiv = false },
                new Torzsvendeg() { TorzsvendegID = 004, Nev = "Róbert Bence", TvEmail = "robertbeni@gmail.com", KedvencStilus = "Electronic", Szuletesnap = "01.20.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 005, Nev = "Horváth Tamás", TvEmail = "htamas@gmail.com", KedvencStilus = "Jazz", Szuletesnap = "04.05.", Aktiv = true },
            };
            int id = 0;
            mockedTorzsvendegRepo.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns(list[0]);
            EladoLogic eLogic = new EladoLogic(mockedVasarlasRepo.Object, mockedTorzsvendegRepo.Object);

            // Act
            Torzsvendeg result = eLogic.TorzsvendegGetOne(id);

            // Assert
            // Assert.That(result, Is.EquivalentTo(expectedList));
            mockedTorzsvendegRepo.Verify(repo => repo.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Tests the TestTorzsvendegIDUpdateLogic operation.
        /// </summary>
        [Test]
        public static void TestTorzsvendegIDUpdateLogic()
        {
            // Arrange
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            List<Vasarlas> list = new List<Vasarlas>() { new Vasarlas() { VasarlasID = 000001, Ar = 5880, VetelDatum = "2019.01.05.", AFA = 5880 * 0.25 } };
            int ertek = 0;
            int id = 0;
            mockedVasarlasRepo.Setup(repo => repo.TorzsvendegIDUpdate(It.IsAny<int>(), It.IsAny<int>())).Callback<int, int>((ertek, id) => list[id].TorzsvendegID = ertek);
            EladoLogic eLogic = new EladoLogic(mockedVasarlasRepo.Object, mockedTorzsvendegRepo.Object);

            // Act
            eLogic.TorzsvendegIDUpdateLogic(ertek, id);

            // Arrange
            mockedVasarlasRepo.Verify(repo => repo.TorzsvendegIDUpdate(ertek, id), Times.Once);
        }

        /// <summary>
        /// Tests the TorzsvendegVetelei operation.
        /// </summary>
        [Test]
        public static void TestTorzsvendegVetelei()
        {
            // Arrange
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            List<Vasarlas> vasarlasList = new List<Vasarlas>()
            {
                new Vasarlas() { VasarlasID = 000001, Ar = 5880, VetelDatum = "2019.01.05.", AFA = 5880 * 0.25 },
                new Vasarlas() { VasarlasID = 000002, Ar = 3490, VetelDatum = "2019.01.06.", AFA = 3490 * 0.25 },
                new Vasarlas() { VasarlasID = 000003, Ar = 2970, VetelDatum = "2019.01.06.", AFA = 2970 * 0.25 },
                new Vasarlas() { VasarlasID = 000004, Ar = 3490, VetelDatum = "2019.01.07.", AFA = 3490 * 0.25 },
                new Vasarlas() { VasarlasID = 000005, Ar = 7390, VetelDatum = "2019.01.07.", AFA = 7390 * 0.25 },
            };
            List<Torzsvendeg> torzsvendegList = new List<Torzsvendeg>()
            {
                 new Torzsvendeg() { TorzsvendegID = 001, Nev = "Horváth Péter", TvEmail = "hpeter@gmail.com", KedvencStilus = "Rock", Szuletesnap = "06.05.", Aktiv = true },
                 new Torzsvendeg() { TorzsvendegID = 002, Nev = "Tóth Krisztina", TvEmail = "tkriszt@gmail.com", KedvencStilus = "RnB", Szuletesnap = "09.01.", Aktiv = true },
                 new Torzsvendeg() { TorzsvendegID = 003, Nev = "Kovács Simon", TvEmail = "kosi@freemail.hu", KedvencStilus = "RnB", Szuletesnap = "11.03.", Aktiv = false },
                 new Torzsvendeg() { TorzsvendegID = 004, Nev = "Róbert Bence", TvEmail = "robertbeni@gmail.com", KedvencStilus = "Electronic", Szuletesnap = "01.20.", Aktiv = true },
                 new Torzsvendeg() { TorzsvendegID = 005, Nev = "Horváth Péter", TvEmail = "htamas@gmail.com", KedvencStilus = "Jazz", Szuletesnap = "04.05.", Aktiv = true },
            };
            vasarlasList[0].TorzsvendegID = torzsvendegList[0].TorzsvendegID;
            vasarlasList[1].TorzsvendegID = torzsvendegList[3].TorzsvendegID;
            vasarlasList[2].TorzsvendegID = torzsvendegList[3].TorzsvendegID;
            vasarlasList[3].TorzsvendegID = torzsvendegList[3].TorzsvendegID;
            vasarlasList[4].TorzsvendegID = torzsvendegList[2].TorzsvendegID;
            List<VeteleiResult> expectedList = new List<VeteleiResult>()
            {
                new VeteleiResult(torzsvendegList[0].Nev, 1),
                new VeteleiResult(torzsvendegList[3].Nev, 3),
                new VeteleiResult(torzsvendegList[2].Nev, 1),
            };
            mockedVasarlasRepo.Setup(repoo => repoo.Read()).Returns(vasarlasList.AsQueryable());
            mockedTorzsvendegRepo.Setup(repoo => repoo.Read()).Returns(torzsvendegList.AsQueryable());
            EladoLogic eLogic = new EladoLogic(mockedVasarlasRepo.Object, mockedTorzsvendegRepo.Object);

            // Act
            List<VeteleiResult> result = eLogic.TorzsvendegVetelei().ToList();

            // Assert
            Assert.That(result, Is.EquivalentTo(expectedList));
            mockedVasarlasRepo.Verify(vRepo => vRepo.Read(), Times.Once);
            mockedTorzsvendegRepo.Verify(tRepo => tRepo.Read(), Times.Exactly(2));
        }
    }
}
