﻿// <copyright file="TulajdonosLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LemezBolt.Data;
    using LemezBolt.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for testing the methods in TulajdonosLogic.
    /// </summary>
    [TestFixture]
    internal static class TulajdonosLogicTests
    {
        /// <summary>
        /// Tests the TestAktivUpdateLogic operation.
        /// </summary>
        [Test]
        public static void TestAktivUpdateLogic()
        {
            // Arrange
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Torzsvendeg> list = new List<Torzsvendeg>() { new Torzsvendeg() { TorzsvendegID = 001, Nev = "Horváth Péter", TvEmail = "hpeter@gmail.com", KedvencStilus = "Rock", Szuletesnap = "06.05.", Aktiv = true } };
            bool ertek = false;
            int id = 0;
            mockedTorzsvendegRepo.Setup(repo => repo.AktivUpdate(It.IsAny<bool>(), It.IsAny<int>())).Callback<bool, int>((ertek, id) => list[id].Aktiv = ertek);
            TulajdonosLogic tLogic = new TulajdonosLogic(mockedLemezRepo.Object, mockedTorzsvendegRepo.Object, mockedVasarlasRepo.Object);

            // Act
            tLogic.AktivUpdateLogic(ertek, id);

            // Arrange
            mockedTorzsvendegRepo.Verify(repo => repo.AktivUpdate(ertek, id), Times.Once);
        }

        /// <summary>
        /// Tests the TorzsvendegCreateLogic operation.
        /// </summary>
        [Test]
        public static void TestTorzsvendegCreateLogic()
        {
            // Arrange
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Torzsvendeg> list = new List<Torzsvendeg>();
            Torzsvendeg customer = new Torzsvendeg() { TorzsvendegID = 001, Nev = "Horváth Péter", TvEmail = "hpeter@gmail.com", KedvencStilus = "Rock", Szuletesnap = "06.05.", Aktiv = true };
            mockedTorzsvendegRepo.Setup(repo => repo.TorzsvendegCreate(It.IsAny<Torzsvendeg>())).Callback<Torzsvendeg>(entry => list.Add(entry));
            TulajdonosLogic tLogic = new TulajdonosLogic(mockedLemezRepo.Object, mockedTorzsvendegRepo.Object, mockedVasarlasRepo.Object);

            // Act
            tLogic.TorzsvendegCreateLogic(customer);

            // Arrange
            mockedTorzsvendegRepo.Verify(repo => repo.TorzsvendegCreate(customer), Times.Once);
        }

        /// <summary>
        /// Tests the TorzsvendegDeleteLogic operation.
        /// </summary>
        [Test]
        public static void TestTorzsvendegDeleteLogic()
        {
            // Arrange
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Torzsvendeg> list = new List<Torzsvendeg>() { new Torzsvendeg() { TorzsvendegID = 001, Nev = "Horváth Péter", TvEmail = "hpeter@gmail.com", KedvencStilus = "Rock", Szuletesnap = "06.05.", Aktiv = true } };
            mockedTorzsvendegRepo.Setup(repo => repo.TorzsvendegDelete(It.IsAny<int>())).Callback<int>(entry => list.RemoveAt(0));
            TulajdonosLogic tLogic = new TulajdonosLogic(mockedLemezRepo.Object, mockedTorzsvendegRepo.Object, mockedVasarlasRepo.Object);

            // Act
            tLogic.TorzsvendegDeleteLogic(0);

            // Arrange
            mockedTorzsvendegRepo.Verify(repo => repo.TorzsvendegDelete(0), Times.Once);
        }

        /// <summary>
        /// Tests the LemezDeleteLogic operation.
        /// </summary>
        [Test]
        public static void TestLemezDeleteLogic()
        {
            // Arrange
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Lemez> list = new List<Lemez>() { new Lemez() { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" } };
            mockedLemezRepo.Setup(repo => repo.LemezDelete(It.IsAny<int>())).Callback<int>(entry => list.RemoveAt(0));
            TulajdonosLogic tLogic = new TulajdonosLogic(mockedLemezRepo.Object, mockedTorzsvendegRepo.Object, mockedVasarlasRepo.Object);

            // Act
            tLogic.LemezDeleteLogic(0);

            // Arrange
            mockedLemezRepo.Verify(repo => repo.LemezDelete(0), Times.Once);
        }

        /// <summary>
        /// Tests the VasarlasDeleteLogic operation.
        /// </summary>
        [Test]
        public static void TestVasarlasDeleteLogic()
        {
            // Arrange
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Vasarlas> list = new List<Vasarlas>() { new Vasarlas() { VasarlasID = 000001, Ar = 5880, VetelDatum = "2019.01.05.", AFA = 5880 * 0.25 } };
            mockedVasarlasRepo.Setup(repo => repo.VasarlasDelete(It.IsAny<int>())).Callback<int>(entry => list.RemoveAt(0));
            TulajdonosLogic tLogic = new TulajdonosLogic(mockedLemezRepo.Object, mockedTorzsvendegRepo.Object, mockedVasarlasRepo.Object);

            // Act
            tLogic.VasarlasDeleteLogic(0);

            // Arrange
            mockedVasarlasRepo.Verify(repo => repo.VasarlasDelete(0), Times.Once);
        }

        /// <summary>
        /// Tests the LegnepszerubbLemez operation.
        /// </summary>
        [Test]
        public static void TestLegnepszerubbLemez()
        {
            // Arrange
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Vasarlas> vasarlasList = new List<Vasarlas>()
            {
                new Vasarlas() { VasarlasID = 000001, Ar = 5880, VetelDatum = "2019.01.05.", AFA = 5880 * 0.25 },
                new Vasarlas() { VasarlasID = 000002, Ar = 3490, VetelDatum = "2019.01.06.", AFA = 3490 * 0.25 },
                new Vasarlas() { VasarlasID = 000003, Ar = 2970, VetelDatum = "2019.01.06.", AFA = 2970 * 0.25 },
                new Vasarlas() { VasarlasID = 000004, Ar = 3490, VetelDatum = "2019.01.07.", AFA = 3490 * 0.25 },
                new Vasarlas() { VasarlasID = 000005, Ar = 7390, VetelDatum = "2019.01.07.", AFA = 7390 * 0.25 },
            };
            List<Lemez> lemezList = new List<Lemez>()
            {
                new Lemez() { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" },
                new Lemez() { LemezID = 00002, Stilus = "Rock", Tipus = "LP", Eloado = "Frank Zappa", LemezCim = "Hot Rats", KiadasEve = "1969" },
                new Lemez() { LemezID = 00003, Stilus = "Jazz", Tipus = "LP", Eloado = "Ryo Fukui", LemezCim = "Mellow Dream", KiadasEve = "1977" },
                new Lemez() { LemezID = 00004, Stilus = "Electronic", Tipus = "EP", Eloado = "Carprenter Brut", LemezCim = "EP I", KiadasEve = "2012" },
                new Lemez() { LemezID = 00005, Stilus = "Hip Hop", Tipus = "LP", Eloado = "Nujabes", LemezCim = "Modal Soul", KiadasEve = "2005" },
            };
            vasarlasList[0].LemezID = lemezList[0].LemezID;
            vasarlasList[1].LemezID = lemezList[3].LemezID;
            vasarlasList[2].LemezID = lemezList[3].LemezID;
            vasarlasList[3].LemezID = lemezList[2].LemezID;
            vasarlasList[4].LemezID = lemezList[2].LemezID;
            mockedVasarlasRepo.Setup(repoo => repoo.Read()).Returns(vasarlasList.AsQueryable());
            mockedLemezRepo.Setup(repoo => repoo.Read()).Returns(lemezList.AsQueryable());
            List<Lemez> expectedLemezList = new List<Lemez>()
            {
                new Lemez() { LemezID = 00003, Stilus = "Jazz", Tipus = "LP", Eloado = "Ryo Fukui", LemezCim = "Mellow Dream", KiadasEve = "1977" },
                new Lemez() { LemezID = 00004, Stilus = "Electronic", Tipus = "EP", Eloado = "Carprenter Brut", LemezCim = "EP I", KiadasEve = "2012" },
            };
            TulajdonosLogic tLogic = new TulajdonosLogic(mockedLemezRepo.Object, mockedTorzsvendegRepo.Object, mockedVasarlasRepo.Object);

            // Act
            var result = tLogic.LegnepszerubbLemez().ToList();

            // Assert
            Assert.That(result, Is.EquivalentTo(expectedLemezList));
            mockedVasarlasRepo.Verify(vRepo => vRepo.Read(), Times.Exactly(2));
            mockedLemezRepo.Verify(lRepo => lRepo.Read(), Times.Exactly(3));
            mockedTorzsvendegRepo.Verify(tRepo => tRepo.Read(), Times.Never);
        }

        /// <summary>
        /// Tests the VasarlokNevei operation.
        /// </summary>
        [Test]
        public static void TestVasarlokNevei()
        {
            // Arrange
            Mock<ITorzsvendegRepository> mockedTorzsvendegRepo = new Mock<ITorzsvendegRepository>(MockBehavior.Loose);
            Mock<IVasarlasRepository> mockedVasarlasRepo = new Mock<IVasarlasRepository>(MockBehavior.Loose);
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Vasarlas> vasarlasList = new List<Vasarlas>()
            {
                new Vasarlas() { VasarlasID = 000001, Ar = 5880, VetelDatum = "2019.01.05.", AFA = 5880 * 0.25 },
                new Vasarlas() { VasarlasID = 000002, Ar = 3490, VetelDatum = "2019.01.06.", AFA = 3490 * 0.25 },
                new Vasarlas() { VasarlasID = 000003, Ar = 2970, VetelDatum = "2019.01.06.", AFA = 2970 * 0.25 },
                new Vasarlas() { VasarlasID = 000004, Ar = 3490, VetelDatum = "2019.01.07.", AFA = 3490 * 0.25 },
                new Vasarlas() { VasarlasID = 000005, Ar = 7390, VetelDatum = "2019.01.07.", AFA = 7390 * 0.25 },
            };
            List<Lemez> lemezList = new List<Lemez>()
            {
                new Lemez() { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" },
                new Lemez() { LemezID = 00002, Stilus = "Rock", Tipus = "LP", Eloado = "Frank Zappa", LemezCim = "Hot Rats", KiadasEve = "1969" },
                new Lemez() { LemezID = 00003, Stilus = "Jazz", Tipus = "LP", Eloado = "Ryo Fukui", LemezCim = "Mellow Dream", KiadasEve = "1977" },
                new Lemez() { LemezID = 00004, Stilus = "Electronic", Tipus = "EP", Eloado = "Carprenter Brut", LemezCim = "EP I", KiadasEve = "2012" },
                new Lemez() { LemezID = 00005, Stilus = "Hip Hop", Tipus = "LP", Eloado = "Nujabes", LemezCim = "Modal Soul", KiadasEve = "2005" },
            };
            List<Torzsvendeg> torzsvendegList = new List<Torzsvendeg>()
            {
                new Torzsvendeg() { TorzsvendegID = 001, Nev = "Horváth Péter", TvEmail = "hpeter@gmail.com", KedvencStilus = "Rock", Szuletesnap = "06.05.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 002, Nev = "Tóth Krisztina", TvEmail = "tkriszt@gmail.com", KedvencStilus = "RnB", Szuletesnap = "09.01.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 003, Nev = "Kovács Simon", TvEmail = "kosi@freemail.hu", KedvencStilus = "RnB", Szuletesnap = "11.03.", Aktiv = false },
                new Torzsvendeg() { TorzsvendegID = 004, Nev = "Róbert Bence", TvEmail = "robertbeni@gmail.com", KedvencStilus = "Electronic", Szuletesnap = "01.20.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 005, Nev = "Horváth Tamás", TvEmail = "htamas@gmail.com", KedvencStilus = "Jazz", Szuletesnap = "04.05.", Aktiv = true },
            };
            vasarlasList[0].LemezID = lemezList[0].LemezID;
            vasarlasList[1].LemezID = lemezList[1].LemezID;
            vasarlasList[2].LemezID = lemezList[2].LemezID;
            vasarlasList[3].LemezID = lemezList[2].LemezID;
            vasarlasList[4].LemezID = lemezList[2].LemezID;
            vasarlasList[0].TorzsvendegID = torzsvendegList[4].TorzsvendegID;
            vasarlasList[1].TorzsvendegID = torzsvendegList[3].TorzsvendegID;
            vasarlasList[2].TorzsvendegID = torzsvendegList[0].TorzsvendegID;
            vasarlasList[3].TorzsvendegID = torzsvendegList[1].TorzsvendegID;
            vasarlasList[4].TorzsvendegID = torzsvendegList[2].TorzsvendegID;
            mockedVasarlasRepo.Setup(repoo => repoo.Read()).Returns(vasarlasList.AsQueryable());
            mockedLemezRepo.Setup(repoo => repoo.Read()).Returns(lemezList.AsQueryable());
            mockedTorzsvendegRepo.Setup(repoo => repoo.Read()).Returns(torzsvendegList.AsQueryable());
            List<Torzsvendeg> expectedList = new List<Torzsvendeg>()
            {
                new Torzsvendeg() { TorzsvendegID = 001, Nev = "Horváth Péter", TvEmail = "hpeter@gmail.com", KedvencStilus = "Rock", Szuletesnap = "06.05.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 002, Nev = "Tóth Krisztina", TvEmail = "tkriszt@gmail.com", KedvencStilus = "RnB", Szuletesnap = "09.01.", Aktiv = true },
                new Torzsvendeg() { TorzsvendegID = 003, Nev = "Kovács Simon", TvEmail = "kosi@freemail.hu", KedvencStilus = "RnB", Szuletesnap = "11.03.", Aktiv = false },
            };
            string eloado = "Ryo Fukui";
            TulajdonosLogic tLogic = new TulajdonosLogic(mockedLemezRepo.Object, mockedTorzsvendegRepo.Object, mockedVasarlasRepo.Object);

            // Act
            List<Torzsvendeg> result = tLogic.VasarlokNevei(eloado).ToList();

            // Assert
            Assert.That(result, Is.EquivalentTo(expectedList));
            mockedVasarlasRepo.Verify(vRepo => vRepo.Read(), Times.Once);
            mockedLemezRepo.Verify(lRepo => lRepo.Read(), Times.Once);
            mockedTorzsvendegRepo.Verify(tRepo => tRepo.Read(), Times.Once);
        }
    }
}
