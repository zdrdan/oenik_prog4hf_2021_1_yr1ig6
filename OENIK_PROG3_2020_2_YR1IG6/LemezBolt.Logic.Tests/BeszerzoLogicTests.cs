﻿// <copyright file="BeszerzoLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using LemezBolt.Data;
    using LemezBolt.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for testing the methods in BeszerzoLogic.
    /// </summary>
    [TestFixture]
    internal static class BeszerzoLogicTests
    {
        /// <summary>
        /// Tests the Read operation.
        /// </summary>
        [Test]
        public static void TestLemezRead()
        {
            // Arrange
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Lemez> list = new List<Lemez>()
            {
                new Lemez() { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" },
                new Lemez() { LemezID = 00002, Stilus = "Rock", Tipus = "LP", Eloado = "Frank Zappa", LemezCim = "Hot Rats", KiadasEve = "1969" },
                new Lemez() { LemezID = 00003, Stilus = "Jazz", Tipus = "LP", Eloado = "Ryo Fukui", LemezCim = "Mellow Dream", KiadasEve = "1977" },
                new Lemez() { LemezID = 00004, Stilus = "Electronic", Tipus = "EP", Eloado = "Carprenter Brut", LemezCim = "EP I", KiadasEve = "2012" },
                new Lemez() { LemezID = 00005, Stilus = "Hip Hop", Tipus = "LP", Eloado = "Nujabes", LemezCim = "Modal Soul", KiadasEve = "2005" },
            };
            mockedLemezRepo.Setup(repo => repo.Read()).Returns(list.AsQueryable());
            BeszerzoLogic bLogic = new BeszerzoLogic(mockedLemezRepo.Object);

            // Act
            List<Lemez> result = bLogic.LemezRead().ToList();

            // Assert
            mockedLemezRepo.Verify(repo => repo.Read(), Times.Once);
        }

        /// <summary>
        /// Tests the LemezCreateLogic operation.
        /// </summary>
        [Test]
        public static void TestLemezCreateLogic()
        {
            // Arrange
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Lemez> list = new List<Lemez>();
            Lemez record = new Lemez() { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" };
            mockedLemezRepo.Setup(repo => repo.LemezCreate(It.IsAny<Lemez>())).Callback<Lemez>(entry => list.Add(entry));
            BeszerzoLogic bLogic = new BeszerzoLogic(mockedLemezRepo.Object);

            // Act
            bLogic.LemezCreateLogic(record);

            // Arrange
            mockedLemezRepo.Verify(repo => repo.LemezCreate(record), Times.Once);
        }

        /// <summary>
        /// Tests the TestEloadoUpdateLogic operation.
        /// </summary>
        [Test]
        public static void TestEloadoUpdateLogic()
        {
            // Arrange
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Lemez> list = new List<Lemez>() { new Lemez { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" } };
            string ertek = " ";
            int id = 0;
            mockedLemezRepo.Setup(repo => repo.EloadoUpdate(It.IsAny<string>(), It.IsAny<int>())).Callback<string, int>((ertek, id) => list[id].Eloado = ertek);
            BeszerzoLogic bLogic = new BeszerzoLogic(mockedLemezRepo.Object);

            // Act
            bLogic.EloadoUpdateLogic(ertek, id);

            // Arrange
            mockedLemezRepo.Verify(repo => repo.EloadoUpdate(ertek, id), Times.Once);
        }

        /// <summary>
        /// Tests the TestKiadasEveUpdateLogic operation.
        /// </summary>
        [Test]
        public static void TestKiadasEveUpdateLogic()
        {
            // Arrange
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Lemez> list = new List<Lemez>() { new Lemez { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" } };
            string ertek = " ";
            int id = 0;
            mockedLemezRepo.Setup(repo => repo.KiadasEveUpdate(It.IsAny<string>(), It.IsAny<int>())).Callback<string, int>((ertek, id) => list[id].KiadasEve = ertek);
            BeszerzoLogic bLogic = new BeszerzoLogic(mockedLemezRepo.Object);

            // Act
            bLogic.KiadasEveUpdateLogic(ertek, id);

            // Arrange
            mockedLemezRepo.Verify(repo => repo.KiadasEveUpdate(ertek, id), Times.Once);
        }

        /// <summary>
        /// Tests the TestLemezCimUpdateLogic operation.
        /// </summary>
        [Test]
        public static void TestLemezCimUpdateLogic()
        {
            // Arrange
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Lemez> list = new List<Lemez>() { new Lemez { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" } };
            string ertek = " ";
            int id = 0;
            mockedLemezRepo.Setup(repo => repo.LemezCimUpdate(It.IsAny<string>(), It.IsAny<int>())).Callback<string, int>((ertek, id) => list[id].LemezCim = ertek);
            BeszerzoLogic bLogic = new BeszerzoLogic(mockedLemezRepo.Object);

            // Act
            bLogic.LemezCimUpdateLogic(ertek, id);

            // Arrange
            mockedLemezRepo.Verify(repo => repo.LemezCimUpdate(ertek, id), Times.Once);
        }

        /// <summary>
        /// Tests the TestStilusUpdateLogic operation.
        /// </summary>
        [Test]
        public static void TestStilusUpdateLogic()
        {
            // Arrange
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Lemez> list = new List<Lemez>() { new Lemez { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" } };
            string ertek = " ";
            int id = 0;
            mockedLemezRepo.Setup(repo => repo.StilusUpdate(It.IsAny<string>(), It.IsAny<int>())).Callback<string, int>((ertek, id) => list[id].Stilus = ertek);
            BeszerzoLogic bLogic = new BeszerzoLogic(mockedLemezRepo.Object);

            // Act
            bLogic.StilusUpdateLogic(ertek, id);

            // Arrange
            mockedLemezRepo.Verify(repo => repo.StilusUpdate(ertek, id), Times.Once);
        }

        /// <summary>
        /// Tests the TestTipusUpdateLogic operation.
        /// </summary>
        [Test]
        public static void TestTipusUpdateLogic()
        {
            // Arrange
            Mock<ILemezRepository> mockedLemezRepo = new Mock<ILemezRepository>(MockBehavior.Loose);
            List<Lemez> list = new List<Lemez>() { new Lemez { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" } };
            string ertek = "EP";
            int id = 0;
            mockedLemezRepo.Setup(repo => repo.TipusUpdate(It.IsAny<string>(), It.IsAny<int>())).Callback<string, int>((ertek, id) => list[id].Tipus = ertek);
            BeszerzoLogic bLogic = new BeszerzoLogic(mockedLemezRepo.Object);

            // Act
            bLogic.TipusUpdateLogic(ertek, id);

            // Arrange
            mockedLemezRepo.Verify(repo => repo.TipusUpdate(ertek, id), Times.Once);
        }
    }
}
