﻿// <copyright file="LemezListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// LemezListViewModel class.
    /// </summary>
    public class LemezListViewModel
    {
        /// <summary>
        /// Gets or sets ListOfLemez.
        /// </summary>
        public IEnumerable<Lemez> ListOfLemez { get; set; }

        /// <summary>
        /// Gets or sets EditedLemez.
        /// </summary>
        public Lemez EditedLemez { get; set; }
    }
}
