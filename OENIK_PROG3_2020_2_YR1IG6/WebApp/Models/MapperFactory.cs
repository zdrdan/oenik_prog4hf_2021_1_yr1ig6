﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// MapperFactory class.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Creates a mapper for Lemez.
        /// </summary>
        /// <returns>Returns a mapper instance.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<LemezBolt.Data.Lemez, WebApp.Models.Lemez>().
                    ForMember(dest => dest.LemezID, map => map.MapFrom(src => src.LemezID)).
                    ForMember(dest => dest.Eloado, map => map.MapFrom(src => src.Eloado)).
                    ForMember(dest => dest.LemezCim, map => map.MapFrom(src => src.LemezCim)).
                    ForMember(dest => dest.Stilus, map => map.MapFrom(src => src.Stilus)).
                    ForMember(dest => dest.Tipus, map => map.MapFrom(src => src.Tipus)).
                    ForMember(dest => dest.KiadasEve, map => map.MapFrom(src => src.KiadasEve)).
                    ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}
