﻿// <copyright file="Lemez.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Lemez class.
    /// </summary>
    public class Lemez
    {
        /// <summary>
        /// Gets or sets LemezID.
        /// </summary>
        [Display(Name = "Lemez ID")]
        [Required]
        public int LemezID { get; set; }

        /// <summary>
        /// Gets or sets Stilus.
        /// </summary>
        [Display(Name = "Stílus")]
        public string Stilus { get; set; }

        /// <summary>
        /// Gets or sets Tipus.
        /// </summary>
        [Display(Name = "Típus")]
        [StringLength(2, MinimumLength = 2)]
        public string Tipus { get; set; }

        /// <summary>
        /// Gets or sets Eloado.
        /// </summary>
        [Display(Name = "Előadó")]
        [Required]
        public string Eloado { get; set; }

        /// <summary>
        /// Gets or sets LemezCim.
        /// </summary>
        [Display(Name = "Lemez cím")]
        [Required]
        public string LemezCim { get; set; }

        /// <summary>
        /// Gets or sets KiadasEve.
        /// </summary>
        [Display(Name = "Kiadás éve")]
        [StringLength(4, MinimumLength = 4)]
        public string KiadasEve { get; set; }
    }
}
