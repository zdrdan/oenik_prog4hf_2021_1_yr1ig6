﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebApp.Controllers
{
    /// <summary>
    /// Apiresult class.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether OperationResult is true or false.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}
