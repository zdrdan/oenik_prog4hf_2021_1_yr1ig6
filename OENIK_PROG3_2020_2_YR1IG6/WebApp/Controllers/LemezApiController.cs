﻿// <copyright file="LemezApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using LemezBolt.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// LemezApiController class.
    /// </summary>
    public class LemezApiController : Controller
    {
        private IBeszerzoLogic bLogic;
        private ITulajdonosLogic tLogic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="LemezApiController"/> class.
        /// </summary>
        /// <param name="bLogic">IBeszerzoLogic instance.</param>
        /// <param name="tLogic">ITulajdonosLogic instance.</param>
        /// <param name="mapper">IMapper instance.</param>
        public LemezApiController(IBeszerzoLogic bLogic, ITulajdonosLogic tLogic, IMapper mapper)
        {
            this.bLogic = bLogic;
            this.tLogic = tLogic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Invokes the logic's read method.
        /// </summary>
        /// <returns>IEnumerable collection.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Lemez> GetAll()
        {
            var lemezek = this.bLogic.LemezRead();
            return this.mapper.Map<IQueryable<LemezBolt.Data.Lemez>, List<Models.Lemez>>(lemezek);
        }

        /// <summary>
        /// Deletes one lemez.
        /// </summary>
        /// <param name="id">The ID of the lemez entry.</param>
        /// <returns>ApiResult instance.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneLemez(int id = 20000)
        {
            try
            {
                this.tLogic.LemezDeleteLogic(id);
            }
            catch (IndexOutOfRangeException)
            {
                return new ApiResult() { OperationResult = false };
            }

            return new ApiResult() { OperationResult = true };
        }

        /// <summary>
        /// Adds one lemez to the database.
        /// </summary>
        /// <param name="lemez">Models.Lemez instance.</param>
        /// <returns>ApiREsult instance.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneLemez(Models.Lemez lemez)
        {
            bool success = true;
            try
            {
                this.bLogic.LemezCreateLogic(this.mapper.Map<Models.Lemez, LemezBolt.Data.Lemez>(lemez));
            }
            catch (ArgumentException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modify one lemez from the database.
        /// </summary>
        /// <param name="lemez">Models.Lemez instance.</param>
        /// <returns>ApiResult instance.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneLemez(Models.Lemez lemez)
        {
            if (lemez != null)
            {
                try
                {
                    this.bLogic.EloadoUpdateLogic(lemez.Eloado, lemez.LemezID);
                    this.bLogic.KiadasEveUpdateLogic(lemez.KiadasEve, lemez.LemezID);
                    this.bLogic.LemezCimUpdateLogic(lemez.LemezCim, lemez.LemezID);
                    this.bLogic.StilusUpdateLogic(lemez.Stilus, lemez.LemezID);
                    this.bLogic.TipusUpdateLogic(lemez.Tipus, lemez.LemezID);
                }
                catch (IndexOutOfRangeException)
                {
                    return new ApiResult() { OperationResult = false };
                }

                return new ApiResult() { OperationResult = true };
            }

            return new ApiResult() { OperationResult = false };
        }
    }
}
