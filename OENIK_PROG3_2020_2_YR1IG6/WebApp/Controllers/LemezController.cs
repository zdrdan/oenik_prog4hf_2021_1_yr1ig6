﻿// <copyright file="LemezController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WebApp.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using LemezBolt.Logic;
    using Microsoft.AspNetCore.Mvc;
    using WebApp.Models;

    /// <summary>
    /// LemezController class.
    /// </summary>
    [CLSCompliant(false)]
    public class LemezController : Controller
    {
        private IBeszerzoLogic beszerzoLogic;
        private ITulajdonosLogic tulajLogic;
        private IMapper mapper;
        private LemezListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="LemezController"/> class.
        /// </summary>
        /// <param name="beszerzoLogic">IBeszerzoLogic instance.</param>
        /// <param name="tulajLogic">ITulajdonosLogic instance.</param>
        /// <param name="mapper">IMapper instance.</param>
        public LemezController(IBeszerzoLogic beszerzoLogic, ITulajdonosLogic tulajLogic, IMapper mapper)
        {
            this.beszerzoLogic = beszerzoLogic;
            this.tulajLogic = tulajLogic;
            this.mapper = mapper;

            this.vm = new LemezListViewModel();
            this.vm.EditedLemez = new Models.Lemez();

            var lemezek = beszerzoLogic?.LemezRead();
            this.vm.ListOfLemez = mapper?.Map<IEnumerable<LemezBolt.Data.Lemez>, List<Models.Lemez>>(lemezek);
        }

        /// <summary>
        /// Invokes View.
        /// </summary>
        /// <returns>A ViewResult based on the viewname and the viewmodel.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("LemezIndex", this.vm);
        }

        /// <summary>
        /// Invokes View.
        /// </summary>
        /// <param name="id">LemezID property.</param>
        /// <returns>A ViewResult based on the viewname and the viewmodel.</returns>
        public IActionResult Details(int id)
        {
            return this.View("LemezDetails", this.GetLemezModel(id));
        }

        /// <summary>
        /// Invokes RedirectToAction.
        /// </summary>
        /// <param name="id">LemezID property.</param>
        /// <returns>A RedirectToAction result.</returns>
        public IActionResult Remove(int id)
        {
            this.tulajLogic.LemezDeleteLogic(id);
            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Invokes View.
        /// </summary>
        /// <param name="id">LemezID property.</param>
        /// <returns>A ViewResult based on the viewname and the viewmodel.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedLemez = this.GetLemezModel(id);
            return this.View("LemezIndex", this.vm);
        }

        /// <summary>
        /// Edits or adds a new lemez entry.
        /// </summary>
        /// <param name="lemez">Lemez instance.</param>
        /// <param name="editAction">Name of the action.</param>
        /// <returns>A redirectaction result or a view result baed on whether modelstate is valid and lemez is not null or not.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Lemez lemez, string editAction)
        {
            if (this.ModelState.IsValid && lemez != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    this.beszerzoLogic.LemezCreateLogic(this.mapper.Map<Models.Lemez, LemezBolt.Data.Lemez>(lemez));
                }
                else
                {
                    this.beszerzoLogic.EloadoUpdateLogic(lemez.Eloado, lemez.LemezID);
                    this.beszerzoLogic.KiadasEveUpdateLogic(lemez.KiadasEve, lemez.LemezID);
                    this.beszerzoLogic.LemezCimUpdateLogic(lemez.LemezCim, lemez.LemezID);
                    this.beszerzoLogic.StilusUpdateLogic(lemez.Stilus, lemez.LemezID);
                    this.beszerzoLogic.TipusUpdateLogic(lemez.Tipus, lemez.LemezID);
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedLemez = lemez;
                return this.View("LemezIndex", this.vm);
            }
        }

        private Models.Lemez GetLemezModel(int id)
        {
            LemezBolt.Data.Lemez oneLemez = this.beszerzoLogic.LemezGetOne(id);
            return this.mapper.Map<LemezBolt.Data.Lemez, WebApp.Models.Lemez>(oneLemez);
        }
    }
}
