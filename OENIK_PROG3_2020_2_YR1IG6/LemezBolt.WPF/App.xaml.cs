﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF
{
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Messaging;
    using LemezBolt.Logic;
    using LemezBolt.WPF.UI;
    using LemezBolt.WPF.UILogic;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        private UIFactory factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            this.factory = UIFactory.CreateFactory();
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);
            MyIoc.Instance.Register<ILemezEditorService, EditorServiceViaWindow>();
            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIoc.Instance.Register<IBeszerzoLogic>(() => this.factory.CreateBeszerzoLogic());
            MyIoc.Instance.Register<IEladoLogic>(() => this.factory.CreateEladoLogic());
            MyIoc.Instance.Register<ITulajdonosLogic>(() => this.factory.CreateTulajdonosLogic());
            MyIoc.Instance.Register<ILemezUILogic, LemezUILogic>();
            Messenger.Default.Register<string>(this, "Closing", msg =>
            {
                this.factory.Dispose();
                Messenger.Default.Unregister(this);
            });
        }
    }
}
