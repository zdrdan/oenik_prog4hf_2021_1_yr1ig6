﻿// <copyright file="LemezUI.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF.Data
{
    using System;
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Observable Lemez class for the UI.
    /// </summary>
    [CLSCompliant(false)]
    public class LemezUI : ObservableObject
    {
        private int lemezID;
        private string stilus;
        private string tipus;
        private string eloado;
        private string lemezCim;
        private string kiadasEve;

        /// <summary>
        /// Gets or sets LemezID.
        /// </summary>
        public int LemezID { get => this.lemezID; set => this.Set(ref this.lemezID, value); }

        /// <summary>
        /// Gets or sets Stilus.
        /// </summary>
        public string Stilus { get => this.stilus; set => this.Set(ref this.stilus, value); }

        /// <summary>
        /// Gets or sets Tipus.
        /// </summary>
        public string Tipus { get => this.tipus; set => this.Set(ref this.tipus, value); }

        /// <summary>
        /// Gets or sets Eloado.
        /// </summary>
        public string Eloado { get => this.eloado; set => this.Set(ref this.eloado, value); }

        /// <summary>
        /// Gets or sets LemezCim.
        /// </summary>
        public string LemezCim { get => this.lemezCim; set => this.Set(ref this.lemezCim, value); }

        /// <summary>
        /// Gets or sets KiadasEve.
        /// </summary>
        public string KiadasEve { get => this.kiadasEve; set => this.Set(ref this.kiadasEve, value); }

        /// <summary>
        /// Nem biztos, hogy kell.
        /// </summary>
        /// <param name="l">LemezUI object we'd like to copy from.</param>
        public void CopyFrom(LemezUI l)
        {
            this.GetType().GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(l)));
        }
    }
}
