﻿// <copyright file="LemezEditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF.VM
{
    using System;
    using GalaSoft.MvvmLight;
    using LemezBolt.WPF.Data;

    /// <summary>
    /// The ViewModel class for the editor window.
    /// </summary>
    [CLSCompliant(false)]
    public class LemezEditorViewModel : ViewModelBase
    {
        private LemezUI lemez;

        /// <summary>
        /// Initializes a new instance of the <see cref="LemezEditorViewModel"/> class.
        /// </summary>
        public LemezEditorViewModel()
        {
            this.lemez = new LemezUI();
            if (this.IsInDesignMode)
            {
                this.lemez.Eloado = "tribe";
                this.lemez.LemezCim = "számok1";
                this.lemez.Stilus = "valami";
                this.lemez.Tipus = "LP";
                this.lemez.KiadasEve = "2021";
            }
        }

        /// <summary>
        /// Gets or Sets the LemezUI object that was selected in the main window.
        /// </summary>
        public LemezUI Lemez { get => this.lemez; set => this.Set(ref this.lemez, value); }
    }
}
