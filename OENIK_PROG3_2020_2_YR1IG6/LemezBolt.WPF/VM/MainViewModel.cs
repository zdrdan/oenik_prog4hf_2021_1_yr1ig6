﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF.VM
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using LemezBolt.WPF.Data;
    using LemezBolt.WPF.UILogic;

    /// <summary>
    /// The ViewModel class for the main window.
    /// </summary>
    [CLSCompliant(false)]
    public class MainViewModel : ViewModelBase
    {
        private ILemezUILogic logic;
        private LemezUI lemezSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">An instance of ILemezUILogic.</param>
        public MainViewModel(ILemezUILogic logic)
        {
            this.logic = logic;

            this.LemezGyujtemeny = new ObservableCollection<LemezUI>();
            logic?.RefreshList(this.LemezGyujtemeny);

            this.AddCmd = new RelayCommand(() => this.logic.AddLemez(this.LemezGyujtemeny));
            this.ModCmd = new RelayCommand(() => this.logic.ModLemez(this.LemezSelected));
            this.DelCmd = new RelayCommand(() => this.logic.DelLemez(this.LemezGyujtemeny, this.LemezSelected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ILemezUILogic>())
        {
        }

        /// <summary>
        /// Gets the observable list used by the UI.
        /// </summary>
        public ObservableCollection<LemezUI> LemezGyujtemeny { get; private set; }

        /// <summary>
        /// Gets or sets the selected LemezUI object.
        /// </summary>
        public LemezUI LemezSelected { get => this.lemezSelected; set => this.Set(ref this.lemezSelected, value); }

        /// <summary>
        /// Gets the property for the Add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets the property for the Modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets the property for the Delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }
    }
}
