﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF.UI
{
    using System;
    using LemezBolt.WPF.Data;
    using LemezBolt.WPF.UILogic;

    /// <summary>
    /// An implementation of ILemezEditorService.
    /// </summary>
    [CLSCompliant(false)]
    public class EditorServiceViaWindow : ILemezEditorService
    {
        /// <summary>
        /// Creates a new editor window and passes the to be modified entry to said window.
        /// </summary>
        /// <param name="l">The to be modified entry.</param>
        /// <returns>Returns with true if the modifying was successful.</returns>
        public bool EditLemez(LemezUI l)
        {
            LemezEditorWindow win = new LemezEditorWindow(l);
            return win.ShowDialog() ?? false;
        }
    }
}
