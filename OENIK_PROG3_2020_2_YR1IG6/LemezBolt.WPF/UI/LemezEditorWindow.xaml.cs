﻿// <copyright file="LemezEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF.UI
{
    using System;
    using System.Windows;
    using LemezBolt.WPF.Data;
    using LemezBolt.WPF.VM;

    /// <summary>
    /// Interaction logic for LemezEditorWindow.xaml.
    /// </summary>
    public partial class LemezEditorWindow : Window
    {
        private LemezEditorViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="LemezEditorWindow"/> class.
        /// </summary>
        public LemezEditorWindow()
        {
            this.InitializeComponent();

            this.vM = this.FindResource("VM") as LemezEditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LemezEditorWindow"/> class.
        /// </summary>
        /// <param name="oldLemez">asdasd.</param>
        [CLSCompliant(false)]
        public LemezEditorWindow(LemezUI oldLemez)
            : this()
        {
            this.vM.Lemez = oldLemez;
        }

        /// <summary>
        /// Gets property for Lemez.
        /// </summary>
        [CLSCompliant(false)]
        public LemezUI Lemez { get => this.vM.Lemez; }

        private void OK_CLICK(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CANCEL_CLICK(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
