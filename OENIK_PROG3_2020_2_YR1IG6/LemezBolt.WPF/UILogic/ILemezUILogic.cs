﻿// <copyright file="ILemezUILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF.UILogic
{
    using System;
    using System.Collections.Generic;
    using LemezBolt.WPF.Data;

    /// <summary>
    /// Interface for LemezUILogic.
    /// </summary>
    [CLSCompliant(false)]
    public interface ILemezUILogic
    {
        /// <summary>
        /// This method calls the database's logic's creation method.
        /// </summary>
        /// <param name="list">A collection of Lemez obejects.</param>
        void AddLemez(IList<LemezUI> list);

        /// <summary>
        /// This method calls the database's logic's modifying methods.
        /// </summary>
        /// <param name="lemezToModify">The object we want to modify.</param>
        void ModLemez(LemezUI lemezToModify);

        /// <summary>
        /// Declaring a method for LemezUILogic to implement.
        /// </summary>
        /// <param name="list">A collection of Lemez obejects.</param>
        /// <param name="lemez">The Lemez object we want to delete.</param>
        void DelLemez(IList<LemezUI> list, LemezUI lemez);

        /// <summary>
        /// Declaring a method for LemezUILogic to implement.
        /// </summary>
        /// <param name="list">The UI's list, which will be reset.</param>
        public void RefreshList(IList<LemezUI> list);
    }
}
