﻿// <copyright file="UIFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF
{
    using System;
    using LemezBolt.Data;
    using LemezBolt.Logic;
    using LemezBolt.Repository;

    /// <summary>
    /// The class that instantiates.
    /// </summary>
    public sealed class UIFactory : IDisposable
    {
        private readonly LemezBoltContext adatb;
        private readonly ILemezRepository lemezRepository;
        private readonly ITorzsvendegRepository torzsvendegRepository;
        private readonly IVasarlasRepository vasarlasRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="UIFactory"/> class.
        /// </summary>
        private UIFactory()
        {
            this.adatb = new LemezBoltContext();
            this.lemezRepository = new LemezRepository(this.adatb);
            this.torzsvendegRepository = new TorzsvendegRepository(this.adatb);
            this.vasarlasRepository = new VasarlasRepository(this.adatb);
        }

        /// <summary>
        /// Creating a new Lemez instance.
        /// </summary>
        /// <returns>Lemez instance.</returns>
        public static Lemez CreateLemez()
        {
            return new Lemez();
        }

        /// <summary>
        /// Creating a new Torzsvendeg instance.
        /// </summary>
        /// <returns>Torzsvendeg instance.</returns>
        public static Torzsvendeg CreateTorzsvendeg()
        {
            return new Torzsvendeg();
        }

        /// <summary>
        /// Creating a new Vasarlas instance.
        /// </summary>
        /// <returns>Vasarlas instance.</returns>
        public static Vasarlas CreateVasarlas()
        {
            return new Vasarlas();
        }

        /// <summary>
        /// A method to call the constructor of the Factory class.
        /// </summary>
        /// <returns>Factory instance.</returns>
        public static UIFactory CreateFactory()
        {
            return new UIFactory();
        }

        /// <summary>
        /// Creating a new TulajdonosLogic instance.
        /// </summary>
        /// <returns>ITulajdonosLogic instance.</returns>
        public ITulajdonosLogic CreateTulajdonosLogic()
        {
            return new TulajdonosLogic(this.lemezRepository, this.torzsvendegRepository, this.vasarlasRepository);
        }

        /// <summary>
        /// Creating a new BeszerzoLogic instance.
        /// </summary>
        /// <returns>IBeszerzoLogic instance.</returns>
        public IBeszerzoLogic CreateBeszerzoLogic()
        {
            return new BeszerzoLogic(this.lemezRepository);
        }

        /// <summary>
        /// Creating a new EladoLogic instance.
        /// </summary>
        /// <returns>IEladoLogic instance.</returns>
        public IEladoLogic CreateEladoLogic()
        {
            return new EladoLogic(this.vasarlasRepository, this.torzsvendegRepository);
        }

        /// <summary>
        /// Calls the dispose method of the database referenced in this class. Called when exiting the program.
        /// </summary>
        public void Dispose()
        {
            this.adatb.Dispose();
        }
    }
}
