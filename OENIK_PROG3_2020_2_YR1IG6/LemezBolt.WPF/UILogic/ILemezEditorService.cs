﻿// <copyright file="ILemezEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF.UILogic
{
    using System;
    using LemezBolt.WPF.Data;

    /// <summary>
    /// Interface for LemezEditorService.
    /// </summary>
    [CLSCompliant(false)]
    public interface ILemezEditorService
    {
        /// <summary>
        /// Declaring a method for LemezEditor to implement.
        /// </summary>
        /// <param name="l">Lemez object.</param>
        /// <returns>A true or false boolean value.</returns>
        bool EditLemez(LemezUI l);
    }
}
