﻿// <copyright file="LemezUILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.WPF.UILogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using LemezBolt.Data;
    using LemezBolt.Logic;
    using LemezBolt.WPF.Data;

    /// <summary>
    /// Implementation of the LemezUILogic interface.
    /// </summary>
    [CLSCompliant(false)]
    public class LemezUILogic : ILemezUILogic
    {
        private IBeszerzoLogic beszerzoLogic;
        private ILemezEditorService lemezEditorService;
        private IMessenger messengerService;
        private ITulajdonosLogic tulajdonosLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="LemezUILogic"/> class.
        /// </summary>
        /// <param name="lemezEditorService">An implementation of the ILemezEditorService interface.</param>
        /// <param name="messengerService">An implementation of the IMessenger interface.</param>
        /// <param name="beszerzoLogic">An implementation of the IBeszerzoLogic interface.</param>
        /// <param name="tulajdonosLogic">An implementation of the ITulajdonosLogic interface.</param>
        public LemezUILogic(ILemezEditorService lemezEditorService, IMessenger messengerService, IBeszerzoLogic beszerzoLogic, ITulajdonosLogic tulajdonosLogic)
        {
            this.lemezEditorService = lemezEditorService;
            this.messengerService = messengerService;
            this.beszerzoLogic = beszerzoLogic;
            this.tulajdonosLogic = tulajdonosLogic;
        }

        /// <summary>
        /// Synchronises the UI's list with the database.
        /// </summary>
        /// <param name="list">A list of database entry representative objects which the UI uses for the display.</param>
        public void RefreshList(IList<LemezUI> list)
        {
            IQueryable<Lemez> temp = this.beszerzoLogic.LemezRead();
            list?.Clear();
            foreach (Lemez item in temp)
            {
                list.Add(new LemezUI() { Eloado = item.Eloado, KiadasEve = item.KiadasEve, LemezCim = item.LemezCim, Stilus = item.Stilus, Tipus = item.Tipus, LemezID = item.LemezID });
            }
        }

        /// <summary>
        /// Takes the user's input and creates an entry and passes it to the Logic's inserting method.
        /// </summary>
        /// <param name="list">A list of database entry representative objects which the UI uses for the display.</param>
        public void AddLemez(IList<LemezUI> list)
        {
            LemezUI newLemez = new LemezUI();

            if (this.lemezEditorService.EditLemez(newLemez) == true)
            {
                this.beszerzoLogic.LemezCreateLogic(new Lemez() { Eloado = newLemez.Eloado, KiadasEve = newLemez.KiadasEve, LemezCim = newLemez.LemezCim, Stilus = newLemez.Stilus, Tipus = newLemez.Tipus });
                this.RefreshList(list);
                this.messengerService.Send("Lemez added.", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Addition cancelled.", "LogicResult");
            }
        }

        /// <summary>
        /// Takes the user's input and passes the cancelland entry's id to the Logic's deleting method.
        /// </summary>
        /// <param name="list">A list of database entry representative objects which the UI uses for the display.</param>
        /// <param name="lemez">The cancelland entry.</param>
        public void DelLemez(IList<LemezUI> list, LemezUI lemez)
        {
            if (lemez != null && list != null && list.Remove(lemez))
            {
                this.tulajdonosLogic.LemezDeleteLogic(lemez.LemezID);
                this.messengerService.Send("Deletion successful.", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Deletion failed.", "LogicResult");
            }
        }

        /// <summary>
        /// Takes the user's input and passes the changed properties to the Logic's updating methods.
        /// </summary>
        /// <param name="lemezToModify">The to be modified entry.</param>
        public void ModLemez(LemezUI lemezToModify)
        {
            LemezUI clone = new LemezUI();
            clone.CopyFrom(lemezToModify);
            if (this.lemezEditorService.EditLemez(clone) == true && lemezToModify != null)
            {
                lemezToModify.CopyFrom(clone);
                this.beszerzoLogic.EloadoUpdateLogic(clone.Eloado, clone.LemezID);
                this.beszerzoLogic.KiadasEveUpdateLogic(clone.KiadasEve, clone.LemezID);
                this.beszerzoLogic.LemezCimUpdateLogic(clone.LemezCim, clone.LemezID);
                this.beszerzoLogic.StilusUpdateLogic(clone.Stilus, clone.LemezID);
                this.beszerzoLogic.TipusUpdateLogic(clone.Tipus, clone.LemezID);
                this.messengerService.Send("Lemez modified.", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Modification cancelled.", "LogicResult");
            }
        }
    }
}
