﻿// <copyright file="Torzsvendeg.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Torzsvendeg class/table.
    /// </summary>
    [Table("Torzsvendeg")]
    public partial class Torzsvendeg
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Torzsvendeg"/> class.
        /// </summary>
        public Torzsvendeg()
        {
            this.Vasarlasok = new HashSet<Vasarlas>();
        }

        /// <summary>
        /// Gets or sets torzsvendegID.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TorzsvendegID { get; set; }

        /// <summary>
        /// Gets or sets nev.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets tvEmail.
        /// </summary>
        public string TvEmail { get; set; }

        /// <summary>
        /// Gets or sets kedvencStilus.
        /// </summary>
        public string KedvencStilus { get; set; }

        /// <summary>
        /// Gets or sets szuletesnap.
        /// </summary>
        public string Szuletesnap { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether aktiv.
        /// </summary>
        public bool Aktiv { get; set; }

        /// <summary>
        /// Gets vasarlok.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Vasarlas> Vasarlasok { get; }

        /// <summary>
        /// Returns with true, if the object in the parameter is exactly like the one calling this method.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>A boolean value.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Torzsvendeg)
            {
                Torzsvendeg other = obj as Torzsvendeg;
                return this.TorzsvendegID == other.TorzsvendegID &&
                       this.Nev == other.Nev &&
                       this.TvEmail == other.TvEmail &&
                       this.KedvencStilus == other.KedvencStilus &&
                       this.Szuletesnap == other.Szuletesnap &&
                       this.Aktiv == other.Aktiv;
            }

            return false;
        }

        /// <summary>
        /// A simple implementation of GetHashCode.
        /// </summary>
        /// <returns>The Torzsvendeg object's ID.</returns>
        public override int GetHashCode()
        {
            return this.TorzsvendegID;
        }
    }
}
