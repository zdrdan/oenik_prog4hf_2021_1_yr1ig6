﻿// <copyright file="LemezBoltContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Data
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// LemezBoltDB.
    /// </summary>
    [CLSCompliant(false)]
    public class LemezBoltContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LemezBoltContext"/> class.
        /// </summary>
        public LemezBoltContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LemezBoltContext"/> class.
        /// </summary>
        /// <param name="options">Dbcontext.</param>
        [CLSCompliant(false)]
        public LemezBoltContext(DbContextOptions<LemezBoltContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets lemezek DbSet.
        /// </summary>
        [CLSCompliant(false)]
        public virtual DbSet<Lemez> Lemezek { get; set; }

        /// <summary>
        /// Gets or sets torzsvendegek DbSet.
        /// </summary>
        [CLSCompliant(false)]
        public virtual DbSet<Torzsvendeg> Torzsvendegek { get; set; }

        /// <summary>
        /// Gets or sets vasarlasok DbSet.
        /// </summary>
        public virtual DbSet<Vasarlas> Vasarlasok { get; set; }

        /// <summary>
        /// LazyLoading configuration.
        /// </summary>
        /// <param name="optionsBuilder">is the builder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder == null)
            {
                throw new ArgumentNullException(null);
            }

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies()
                              .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\LemezBoltDB.mdf;Integrated Security=True");
            }
        }

        /// <summary>
        /// Creating Database model.
        /// </summary>
        /// <param name="modelBuilder"> is the builder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentNullException(null);
            }

            Lemez l1 = new Lemez() { LemezID = 00001, Stilus = "Rock", Tipus = "LP", Eloado = "Led Zeppelin", LemezCim = "Led Zeppelin", KiadasEve = "1969" };
            Lemez l2 = new Lemez() { LemezID = 00002, Stilus = "Rock", Tipus = "LP", Eloado = "Frank Zappa", LemezCim = "Hot Rats", KiadasEve = "1969" };
            Lemez l3 = new Lemez() { LemezID = 00003, Stilus = "Jazz", Tipus = "LP", Eloado = "Ryo Fukui", LemezCim = "Mellow Dream", KiadasEve = "1977" };
            Lemez l4 = new Lemez() { LemezID = 00004, Stilus = "Electronic", Tipus = "EP", Eloado = "Carprenter Brut", LemezCim = "EP I", KiadasEve = "2012" };
            Lemez l5 = new Lemez() { LemezID = 00005, Stilus = "Hip Hop", Tipus = "LP", Eloado = "Nujabes", LemezCim = "Modal Soul", KiadasEve = "2005" };
            Lemez l6 = new Lemez() { LemezID = 00006, Stilus = "Electronic", Tipus = "EP", Eloado = "Gesaffelstein", LemezCim = "Conspiracy Pt.1", KiadasEve = "2011" };
            Lemez l7 = new Lemez() { LemezID = 00007, Stilus = "Electronic", Tipus = "EP", Eloado = "Gesaffelstein", LemezCim = "Conspiracy Pt.2", KiadasEve = "2011" };
            Lemez l8 = new Lemez() { LemezID = 00008, Stilus = "Metal", Tipus = "LP", Eloado = "Metallica", LemezCim = "Ride The Lightning", KiadasEve = "1984" };
            Lemez l9 = new Lemez() { LemezID = 00009, Stilus = "Metal", Tipus = "LP", Eloado = "Megadeth", LemezCim = "Rust In Peace", KiadasEve = "1990" };
            Lemez l10 = new Lemez() { LemezID = 00010, Stilus = "Electronic", Tipus = "LP", Eloado = "Justice", LemezCim = "†(Cross)", KiadasEve = "2007" };
            Lemez l11 = new Lemez() { LemezID = 00011, Stilus = "Grunge", Tipus = "LP", Eloado = "Nirvana", LemezCim = "In Utero", KiadasEve = "1993" };
            Lemez l12 = new Lemez() { LemezID = 00012, Stilus = "Hip Hop", Tipus = "EP", Eloado = "Young Thug", LemezCim = "Young Martha", KiadasEve = "2017" };
            Lemez l13 = new Lemez() { LemezID = 00013, Stilus = "Hip Hop", Tipus = "LP", Eloado = "A Tribe Called Quest", LemezCim = "People's Instinctive Travels and the Paths of Rhythm", KiadasEve = "1990" };
            Lemez l14 = new Lemez() { LemezID = 00014, Stilus = "RnB", Tipus = "LP", Eloado = "Frank Ocean", LemezCim = "Channel Orange", KiadasEve = "2012" };
            Lemez l15 = new Lemez() { LemezID = 00015, Stilus = "RnB", Tipus = "LP", Eloado = "Jai Paul", LemezCim = "Leak 04-13 (Bait Ones)", KiadasEve = "2019" };

            Torzsvendeg t1 = new Torzsvendeg() { TorzsvendegID = 001, Nev = "Horváth Péter", TvEmail = "hpeter@gmail.com", KedvencStilus = "Rock", Szuletesnap = "06.05.", Aktiv = true };
            Torzsvendeg t2 = new Torzsvendeg() { TorzsvendegID = 002, Nev = "Tóth Krisztina", TvEmail = "tkriszt@gmail.com", KedvencStilus = "RnB", Szuletesnap = "09.01.", Aktiv = true };
            Torzsvendeg t3 = new Torzsvendeg() { TorzsvendegID = 003, Nev = "Kovács Simon", TvEmail = "kosi@freemail.hu", KedvencStilus = "RnB", Szuletesnap = "11.03.", Aktiv = false };
            Torzsvendeg t4 = new Torzsvendeg() { TorzsvendegID = 004, Nev = "Róbert Bence", TvEmail = "robertbeni@gmail.com", KedvencStilus = "Electronic", Szuletesnap = "01.20.", Aktiv = true };
            Torzsvendeg t5 = new Torzsvendeg() { TorzsvendegID = 005, Nev = "Horváth Péter", TvEmail = "htamas@gmail.com", KedvencStilus = "Jazz", Szuletesnap = "04.05.", Aktiv = true };
            Torzsvendeg t6 = new Torzsvendeg() { TorzsvendegID = 006, Nev = "Nagy Hanna", TvEmail = "nahanna@gmail.com", KedvencStilus = "Hip Hop", Szuletesnap = "08.31.", Aktiv = true };
            Torzsvendeg t7 = new Torzsvendeg() { TorzsvendegID = 007, Nev = "Szabó Dominik", TvEmail = "szabodom@freemail.hu", KedvencStilus = "Rock", Szuletesnap = "05.10.", Aktiv = true };
            Torzsvendeg t8 = new Torzsvendeg() { TorzsvendegID = 008, Nev = "Kiss Botond", TvEmail = "kissboti@gmail.com", KedvencStilus = "Rock", Szuletesnap = "11.23.", Aktiv = true };
            Torzsvendeg t9 = new Torzsvendeg() { TorzsvendegID = 009, Nev = "Németh Zsombor", TvEmail = "zsombornemeth@gmail.com", KedvencStilus = "Hip Hop", Szuletesnap = "06.24.", Aktiv = true };
            Torzsvendeg t10 = new Torzsvendeg() { TorzsvendegID = 010, Nev = "Farkas Áron", TvEmail = "farkasa@gmail.com", KedvencStilus = "Rock", Szuletesnap = "08.29.", Aktiv = false };
            Torzsvendeg t11 = new Torzsvendeg() { TorzsvendegID = 011, Nev = "Papp Emma", TvEmail = "pappemm@gmail.com", KedvencStilus = "Jazz", Szuletesnap = "09.26.", Aktiv = true };
            Torzsvendeg t12 = new Torzsvendeg() { TorzsvendegID = 012, Nev = "Oláh Botond", TvEmail = "olahboti@freemail.hu", KedvencStilus = "Hip Hop", Szuletesnap = "10.07.", Aktiv = true };
            Torzsvendeg t13 = new Torzsvendeg() { TorzsvendegID = 013, Nev = "Rácz Simon", TvEmail = "raczsimon99@citromail.hu", KedvencStilus = "Hip Hop", Szuletesnap = "07.12.", Aktiv = true };
            Torzsvendeg t14 = new Torzsvendeg() { TorzsvendegID = 014, Nev = "Juhász Dóra", TvEmail = "judora98@gmail.com", KedvencStilus = "Electronic", Szuletesnap = "03.15.", Aktiv = true };
            Torzsvendeg t15 = new Torzsvendeg() { TorzsvendegID = 015, Nev = "Budai Viktória", TvEmail = "budaiviktoria@citromail.hu", KedvencStilus = "Hip Hop", Szuletesnap = "12.17.", Aktiv = true };
            Torzsvendeg t16 = new Torzsvendeg() { TorzsvendegID = 016, Nev = "Szilágyi László", TvEmail = "szilasz@freemail.hu", KedvencStilus = "Electronic", Szuletesnap = "06.29.", Aktiv = false };

            Vasarlas v1 = new Vasarlas() { VasarlasID = 000001, Ar = 5880, VetelDatum = "2019.01.05.", AFA = 5880 * 0.25 };
            Vasarlas v2 = new Vasarlas() { VasarlasID = 000002, Ar = 3490, VetelDatum = "2019.01.06.", AFA = 3490 * 0.25 };
            Vasarlas v3 = new Vasarlas() { VasarlasID = 000003, Ar = 2970, VetelDatum = "2019.01.06.", AFA = 2970 * 0.25 };
            Vasarlas v4 = new Vasarlas() { VasarlasID = 000004, Ar = 3490, VetelDatum = "2019.01.07.", AFA = 3490 * 0.25 };
            Vasarlas v5 = new Vasarlas() { VasarlasID = 000005, Ar = 7390, VetelDatum = "2019.01.07.", AFA = 7390 * 0.25 };
            Vasarlas v6 = new Vasarlas() { VasarlasID = 000006, Ar = 7690, VetelDatum = "2019.01.13.", AFA = 7690 * 0.25 };
            Vasarlas v7 = new Vasarlas() { VasarlasID = 000007, Ar = 5768, VetelDatum = "2019.01.14.", AFA = 5768 * 0.25 };
            Vasarlas v8 = new Vasarlas() { VasarlasID = 000008, Ar = 4190, VetelDatum = "2019.01.16.", AFA = 4190 * 0.25 };
            Vasarlas v9 = new Vasarlas() { VasarlasID = 000009, Ar = 3990, VetelDatum = "2019.01.16.", AFA = 3990 * 0.25 };
            Vasarlas v10 = new Vasarlas() { VasarlasID = 000010, Ar = 3990, VetelDatum = "2019.01.16.", AFA = 3990 * 0.25 };
            Vasarlas v11 = new Vasarlas() { VasarlasID = 000011, Ar = 4190, VetelDatum = "2019.02.01.", AFA = 4190 * 0.25 };
            Vasarlas v12 = new Vasarlas() { VasarlasID = 000012, Ar = 5880, VetelDatum = "2019.02.01.", AFA = 5880 * 0.25 };
            Vasarlas v13 = new Vasarlas() { VasarlasID = 000013, Ar = 3990, VetelDatum = "2019.02.01.", AFA = 3990 * 0.25 };
            Vasarlas v14 = new Vasarlas() { VasarlasID = 000014, Ar = 7810, VetelDatum = "2019.02.02.", AFA = 7810 * 0.25 };
            Vasarlas v15 = new Vasarlas() { VasarlasID = 000015, Ar = 7810, VetelDatum = "2019.02.08.", AFA = 7810 * 0.25 };
            Vasarlas v16 = new Vasarlas() { VasarlasID = 000016, Ar = 3470, VetelDatum = "2019.02.10.", AFA = 3470 * 0.25 };
            Vasarlas v17 = new Vasarlas() { VasarlasID = 000017, Ar = 4190, VetelDatum = "2019.02.12.", AFA = 4190 * 0.25 };

            v1.LemezID = l1.LemezID;
            v2.LemezID = l14.LemezID;
            v3.LemezID = l15.LemezID;
            v4.LemezID = l14.LemezID;
            v5.LemezID = l6.LemezID;
            v6.LemezID = l7.LemezID;
            v7.LemezID = l3.LemezID;
            v8.LemezID = l13.LemezID;
            v9.LemezID = l2.LemezID;
            v10.LemezID = l2.LemezID;
            v11.LemezID = l13.LemezID;
            v12.LemezID = l1.LemezID;
            v13.LemezID = l2.LemezID;
            v14.LemezID = l5.LemezID;
            v15.LemezID = l5.LemezID;
            v16.LemezID = l4.LemezID;
            v17.LemezID = l13.LemezID;

            v1.TorzsvendegID = t1.TorzsvendegID;
            v2.TorzsvendegID = t2.TorzsvendegID;
            v3.TorzsvendegID = t2.TorzsvendegID;
            v4.TorzsvendegID = t4.TorzsvendegID;
            v5.TorzsvendegID = t4.TorzsvendegID;
            v6.TorzsvendegID = t4.TorzsvendegID;
            v7.TorzsvendegID = t5.TorzsvendegID;
            v8.TorzsvendegID = t6.TorzsvendegID;
            v9.TorzsvendegID = t7.TorzsvendegID;
            v10.TorzsvendegID = t8.TorzsvendegID;
            v11.TorzsvendegID = t9.TorzsvendegID;
            v12.TorzsvendegID = t9.TorzsvendegID;
            v13.TorzsvendegID = t11.TorzsvendegID;
            v14.TorzsvendegID = t12.TorzsvendegID;
            v15.TorzsvendegID = t13.TorzsvendegID;
            v16.TorzsvendegID = t14.TorzsvendegID;
            v17.TorzsvendegID = t15.TorzsvendegID;

            modelBuilder.Entity<Vasarlas>(entity =>
            {
                entity.HasOne(vasarlas => vasarlas.Lemez)
                      .WithMany(lemez => lemez.Vasarlasok)
                      .HasForeignKey(vasarlas => vasarlas.LemezID)
                      .OnDelete(DeleteBehavior.ClientSetNull);
                entity.HasOne(vasarlas => vasarlas.Torzsvendeg)
                      .WithMany(torzsvendeg => torzsvendeg.Vasarlasok)
                      .HasForeignKey(vasarlas => vasarlas.TorzsvendegID)
                      .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Lemez>().HasData(l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15);
            modelBuilder.Entity<Torzsvendeg>().HasData(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16);
            modelBuilder.Entity<Vasarlas>().HasData(v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15, v16, v17);
        }
    }
}
