﻿// <copyright file="Lemez.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Lemez class/table.
    /// </summary>
    [Table("Lemez")]
    public partial class Lemez
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Lemez"/> class.
        /// </summary>
        public Lemez()
        {
            this.Vasarlasok = new HashSet<Vasarlas>();
        }

        /// <summary>
        /// Gets or sets LemezID.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LemezID { get; set; }

        /// <summary>
        /// Gets or sets Stilus.
        /// </summary>
        public string Stilus { get; set; }

        /// <summary>
        /// Gets or sets Tipus.
        /// </summary>
        public string Tipus { get; set; }

        /// <summary>
        /// Gets or sets Eloado.
        /// </summary>
        public string Eloado { get; set; }

        /// <summary>
        /// Gets or sets LemezCim.
        /// </summary>
        public string LemezCim { get; set; }

        /// <summary>
        /// Gets or sets KiadasEve.
        /// </summary>
        public string KiadasEve { get; set; }

        /// <summary>
        /// Gets Vasarlasok.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Vasarlas> Vasarlasok { get; }

        /// <summary>
        /// Returns with true, if the object in the parameter is exactly like the one calling this method.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>A boolean value.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Lemez)
            {
                Lemez other = obj as Lemez;
                return this.LemezID == other.LemezID &&
                       this.Eloado == other.Eloado &&
                       this.LemezCim == other.LemezCim &&
                       this.Stilus == other.Stilus &&
                       this.Tipus == other.Tipus &&
                       this.KiadasEve == other.KiadasEve;
            }

            return false;
        }

        /// <summary>
        /// A simple implementation of GetHashCode.
        /// </summary>
        /// <returns>The Lemez object's ID.</returns>
        public override int GetHashCode()
        {
            return this.LemezID;
        }
    }
}