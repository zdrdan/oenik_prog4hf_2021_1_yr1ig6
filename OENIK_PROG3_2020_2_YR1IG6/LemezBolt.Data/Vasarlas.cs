﻿// <copyright file="Vasarlas.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace LemezBolt.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Vasarlas class/table.
    /// </summary>
    [Table("Vasarlas")]
    public partial class Vasarlas
    {
        /// <summary>
        /// Gets or sets vasarlasID.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VasarlasID { get; set; }

        /// <summary>
        /// Gets or sets lemezID.
        /// </summary>
        [ForeignKey(nameof(Lemez))]
        public int LemezID { get; set; }

        /// <summary>
        /// Gets or sets torzsvendegID.
        /// </summary>
        [ForeignKey(nameof(Torzsvendeg))]
        public int TorzsvendegID { get; set; }

        /// <summary>
        /// Gets or sets ar.
        /// </summary>
        public int Ar { get; set; }

        /// <summary>
        /// Gets or sets VetelDatum.
        /// </summary>
        public string VetelDatum { get; set; }

        /// <summary>
        /// Gets or sets AFA.
        /// </summary>
        public double AFA { get; set; }

        /// <summary>
        /// Gets or sets Lemez.
        /// </summary>
        [NotMapped]
        public virtual Lemez Lemez { get; set; }

        /// <summary>
        /// Gets or sets torzsvendeg.
        /// </summary>
        [NotMapped]
        public virtual Torzsvendeg Torzsvendeg { get; set; }

        /// <summary>
        /// Returns with true, if the object in the parameter is exactly like the one calling this method.
        /// </summary>
        /// <param name="obj">The other object.</param>
        /// <returns>A boolean value.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Vasarlas)
            {
                Vasarlas other = obj as Vasarlas;
                return this.LemezID == other.LemezID &&
                       this.TorzsvendegID == other.TorzsvendegID &&
                       this.VasarlasID == other.VasarlasID &&
                       this.Ar == other.Ar &&
                       this.AFA == other.AFA &&
                       this.VetelDatum == other.VetelDatum;
            }

            return false;
        }

        /// <summary>
        /// A simple implementation of GetHashCode.
        /// </summary>
        /// <returns>The Vasarlas object's ID.</returns>
        public override int GetHashCode()
        {
            return this.VasarlasID;
        }
    }
}
