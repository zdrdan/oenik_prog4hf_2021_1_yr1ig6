var interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository =
[
    [ "AktivUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html#a2e9fccdde3baec65cd7f98fbbbd84ab2", null ],
    [ "KedvencStilusUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html#a1016f987002b7f756aa769f3368dad81", null ],
    [ "NevUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html#a3fe5337d68d902903d3ef54a65f6ac8d", null ],
    [ "SzuletesnapUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html#ab0fe4ca5f2a84881cb072358ca27cfab", null ],
    [ "TorzsvendegCreate", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html#ab4e1bbfeb9e26160217e372ed5eb65cc", null ],
    [ "TorzsvendegDelete", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html#afaa2975d2f549fe830bf4f86b0c5f33e", null ],
    [ "TvEmailUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html#ad0c69a730547308d476bd3b73d59b83e", null ]
];