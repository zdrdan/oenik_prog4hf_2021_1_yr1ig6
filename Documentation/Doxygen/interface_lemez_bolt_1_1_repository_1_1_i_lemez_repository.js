var interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository =
[
    [ "EloadoUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html#a07fd78de7bb09298c1dd1f042df70a05", null ],
    [ "KiadasEveUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html#a2c56a5117f480a8066457ff4e1a6e96d", null ],
    [ "LemezCimUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html#a48d62559fd5462bc2f59338347ee7972", null ],
    [ "LemezCreate", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html#a3332812439fce4f7d3e12b2570e81442", null ],
    [ "LemezDelete", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html#a234309095faa1e7bc32b6ccd3ba1385b", null ],
    [ "StilusUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html#a098fb1da827d2096151ce42053c26227", null ],
    [ "TipusUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html#a5308dcb898a896b93c4dd04c7dc2b4a3", null ]
];