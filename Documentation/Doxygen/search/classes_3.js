var searchData=
[
  ['ibeszerzologic_170',['IBeszerzoLogic',['../interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html',1,'LemezBolt::Logic']]],
  ['ieladologic_171',['IEladoLogic',['../interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html',1,'LemezBolt::Logic']]],
  ['ilemezrepository_172',['ILemezRepository',['../interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html',1,'LemezBolt::Repository']]],
  ['irepository_173',['IRepository',['../interface_lemez_bolt_1_1_repository_1_1_i_repository.html',1,'LemezBolt::Repository']]],
  ['irepository_3c_20lemez_20_3e_174',['IRepository&lt; Lemez &gt;',['../interface_lemez_bolt_1_1_repository_1_1_i_repository.html',1,'LemezBolt::Repository']]],
  ['irepository_3c_20torzsvendeg_20_3e_175',['IRepository&lt; Torzsvendeg &gt;',['../interface_lemez_bolt_1_1_repository_1_1_i_repository.html',1,'LemezBolt::Repository']]],
  ['irepository_3c_20vasarlas_20_3e_176',['IRepository&lt; Vasarlas &gt;',['../interface_lemez_bolt_1_1_repository_1_1_i_repository.html',1,'LemezBolt::Repository']]],
  ['itorzsvendegrepository_177',['ITorzsvendegRepository',['../interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html',1,'LemezBolt::Repository']]],
  ['itulajdonoslogic_178',['ITulajdonosLogic',['../interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html',1,'LemezBolt::Logic']]],
  ['ivasarlasrepository_179',['IVasarlasRepository',['../interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html',1,'LemezBolt::Repository']]]
];
