var interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic =
[
    [ "EloadoUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html#a277d5d950a9da1a7759b3c47eec6bc38", null ],
    [ "KiadasEveUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html#ac3c82f86ee99e03b9a5d68c9835bff3e", null ],
    [ "LemezCimUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html#af6f6f307b5e86ca91336575c7717b250", null ],
    [ "LemezCreateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html#aa3dbe4a589f2f49cb0ea2d53609c8922", null ],
    [ "LemezGetOne", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html#a51e1198d40f6319999daaefcc1cced67", null ],
    [ "LemezRead", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html#a13b197450864f432477dfa00dfb3beab", null ],
    [ "StilusUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html#a703ea534d5450510cbb49bf244015a25", null ],
    [ "TipusUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html#acd5694d5f53a02905ac588f18ebeece5", null ]
];