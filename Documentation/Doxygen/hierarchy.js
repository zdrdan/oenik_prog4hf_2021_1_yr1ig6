var hierarchy =
[
    [ "LemezBolt.Logic.Tests.BeszerzoLogicTests", "class_lemez_bolt_1_1_logic_1_1_tests_1_1_beszerzo_logic_tests.html", null ],
    [ "DbContext", null, [
      [ "LemezBolt.Data.LemezBoltContext", "class_lemez_bolt_1_1_data_1_1_lemez_bolt_context.html", null ]
    ] ],
    [ "LemezBolt.Logic.Tests.EladoLogicTests", "class_lemez_bolt_1_1_logic_1_1_tests_1_1_elado_logic_tests.html", null ],
    [ "LemezBolt.Logic.IBeszerzoLogic", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html", [
      [ "LemezBolt.Logic.BeszerzoLogic", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "LemezBolt.Program.Factory", "class_lemez_bolt_1_1_program_1_1_factory.html", null ]
    ] ],
    [ "LemezBolt.Logic.IEladoLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html", [
      [ "LemezBolt.Logic.EladoLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html", null ]
    ] ],
    [ "LemezBolt.Repository.IRepository< T >", "interface_lemez_bolt_1_1_repository_1_1_i_repository.html", [
      [ "LemezBolt.Repository.ParentRepository< T >", "class_lemez_bolt_1_1_repository_1_1_parent_repository.html", null ]
    ] ],
    [ "LemezBolt.Repository.IRepository< Lemez >", "interface_lemez_bolt_1_1_repository_1_1_i_repository.html", [
      [ "LemezBolt.Repository.ILemezRepository", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html", [
        [ "LemezBolt.Repository.LemezRepository", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html", null ]
      ] ]
    ] ],
    [ "LemezBolt.Repository.IRepository< Torzsvendeg >", "interface_lemez_bolt_1_1_repository_1_1_i_repository.html", [
      [ "LemezBolt.Repository.ITorzsvendegRepository", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html", [
        [ "LemezBolt.Repository.TorzsvendegRepository", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html", null ]
      ] ]
    ] ],
    [ "LemezBolt.Repository.IRepository< Vasarlas >", "interface_lemez_bolt_1_1_repository_1_1_i_repository.html", [
      [ "LemezBolt.Repository.IVasarlasRepository", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html", [
        [ "LemezBolt.Repository.VasarlasRepository", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html", null ]
      ] ]
    ] ],
    [ "LemezBolt.Logic.ITulajdonosLogic", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html", [
      [ "LemezBolt.Logic.TulajdonosLogic", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html", null ]
    ] ],
    [ "LemezBolt.Data.Lemez", "class_lemez_bolt_1_1_data_1_1_lemez.html", null ],
    [ "LemezBolt.Program.Methods", "class_lemez_bolt_1_1_program_1_1_methods.html", null ],
    [ "LemezBolt.Repository.ParentRepository< Lemez >", "class_lemez_bolt_1_1_repository_1_1_parent_repository.html", [
      [ "LemezBolt.Repository.LemezRepository", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html", null ]
    ] ],
    [ "LemezBolt.Repository.ParentRepository< Torzsvendeg >", "class_lemez_bolt_1_1_repository_1_1_parent_repository.html", [
      [ "LemezBolt.Repository.TorzsvendegRepository", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html", null ]
    ] ],
    [ "LemezBolt.Repository.ParentRepository< Vasarlas >", "class_lemez_bolt_1_1_repository_1_1_parent_repository.html", [
      [ "LemezBolt.Repository.VasarlasRepository", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html", null ]
    ] ],
    [ "LemezBolt.Program.Program", "class_lemez_bolt_1_1_program_1_1_program.html", null ],
    [ "LemezBolt.Program.SzovegRescources", "class_lemez_bolt_1_1_program_1_1_szoveg_rescources.html", null ],
    [ "LemezBolt.Data.Torzsvendeg", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html", null ],
    [ "LemezBolt.Logic.Tests.TulajdonosLogicTests", "class_lemez_bolt_1_1_logic_1_1_tests_1_1_tulajdonos_logic_tests.html", null ],
    [ "LemezBolt.Data.Vasarlas", "class_lemez_bolt_1_1_data_1_1_vasarlas.html", null ],
    [ "LemezBolt.Logic.VeteleiResult", "class_lemez_bolt_1_1_logic_1_1_vetelei_result.html", null ]
];