var class_lemez_bolt_1_1_logic_1_1_beszerzo_logic =
[
    [ "BeszerzoLogic", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html#a010e907deff6fc58015e687b64f00c93", null ],
    [ "EloadoUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html#a7bb8e8ae531fcc0ffdbcebdc258872eb", null ],
    [ "KiadasEveUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html#ab6bf138bd6d1ead227af417bc66083e4", null ],
    [ "LemezCimUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html#a2f9ab7ef7fe052d27e23ed75bc23a5cb", null ],
    [ "LemezCreateLogic", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html#ad6beb7756e805e98448dc0915ccd5413", null ],
    [ "LemezGetOne", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html#aa4c868f78472b8470ebe08d31e68c0ba", null ],
    [ "LemezRead", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html#a08e5c40e36fa9296015e80a16606dbf4", null ],
    [ "StilusUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html#a2e82a26102b72a80107f9e792ef71c3e", null ],
    [ "TipusUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html#a5016362f6477b8584ff108f89b1ec65d", null ]
];