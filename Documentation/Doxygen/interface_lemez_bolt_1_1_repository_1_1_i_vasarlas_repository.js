var interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository =
[
    [ "AFAUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html#a13ecaa99e98950dda535d64869983625", null ],
    [ "ArUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html#ab5cfd2d962b172919a7c532cc79a2e98", null ],
    [ "LemezIDUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html#a387efe45d0c8d21b7b5dcf66f7a5bbe2", null ],
    [ "TorzsvendegIDUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html#a7990b1b2f80aa661590f492d33092904", null ],
    [ "VasarlasCreate", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html#aa75447e9272a1c1aa4d790eef4a17e8f", null ],
    [ "VasarlasDelete", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html#a8dea9e939a161c88ecb111561578e30a", null ],
    [ "VetelDatumUpdate", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html#aac70d70b7e687996929744b7c14aebea", null ]
];