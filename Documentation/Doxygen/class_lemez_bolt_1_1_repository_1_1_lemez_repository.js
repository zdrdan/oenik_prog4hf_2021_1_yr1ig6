var class_lemez_bolt_1_1_repository_1_1_lemez_repository =
[
    [ "LemezRepository", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html#a666e5db3d1f3375f40659f060abaf50c", null ],
    [ "EloadoUpdate", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html#a323de19be2ade2cd6a3ebad53326054d", null ],
    [ "GetOne", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html#ac56a8e87972d7b3fe38b3405a240d45d", null ],
    [ "KiadasEveUpdate", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html#aaddb50c356506e5e61eef8d22ddfea7a", null ],
    [ "LemezCimUpdate", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html#a0aa060e42e8d80028c2ee19b60ee278b", null ],
    [ "LemezCreate", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html#aff73e2df5173c8dfc18c500ba2d41805", null ],
    [ "LemezDelete", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html#afad6bab9ffd6b879447480c89131481c", null ],
    [ "StilusUpdate", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html#a9e3658807a4dec3a50afad83248691b6", null ],
    [ "TipusUpdate", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html#a2a4240c737b3610184780e8239ab05f8", null ]
];