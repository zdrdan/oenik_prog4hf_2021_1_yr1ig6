var class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository =
[
    [ "TorzsvendegRepository", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html#a63a54a98352d9761a65f0e34d6352135", null ],
    [ "AktivUpdate", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html#a200c3e82a17fbc09aba7f6fccd488518", null ],
    [ "GetOne", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html#abb3c9228361e732423614173d4fd04a0", null ],
    [ "KedvencStilusUpdate", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html#a0e401612d878778e84c87586d8e90ef4", null ],
    [ "NevUpdate", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html#ad7273427c006c4a2cad96d0937bfdb49", null ],
    [ "SzuletesnapUpdate", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html#a1eecd685f12469371959cdf06ecc23dc", null ],
    [ "TorzsvendegCreate", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html#ac8bece60720a5ba172362c575ee95321", null ],
    [ "TorzsvendegDelete", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html#a1774f95a5dc48fe9333f371da3e84567", null ],
    [ "TvEmailUpdate", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html#aa4e02ec1dbbf11b4573dd44271ea4d77", null ]
];