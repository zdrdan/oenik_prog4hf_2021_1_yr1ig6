var class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic =
[
    [ "TulajdonosLogic", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#a32187b4cc4b035e42e0173b7de5273b6", null ],
    [ "AktivUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#a59a4ca92cf0a5eb993fea898b4efc143", null ],
    [ "LegnepszerubbLemez", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#a8ddb6fca0f6b12c2e6efe694c6bd5ccc", null ],
    [ "LegnepszerubbLemezAsync", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#aca07044e2d1fe57511024e0777add6f6", null ],
    [ "LemezDeleteLogic", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#ac32eeeb91a3e5639b95382dac083359b", null ],
    [ "TorzsvendegCreateLogic", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#aa01a0924335073b69216fd25696faf02", null ],
    [ "TorzsvendegDeleteLogic", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#a8fefe8ba743fab3ddbf90aa96aa2fb1b", null ],
    [ "VasarlasDeleteLogic", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#a72274d2e4869b3baa693e93505a41130", null ],
    [ "VasarlokNevei", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#a1fb4e80d1942025615728cbcc71a6e76", null ],
    [ "VasarlokNeveiAsync", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html#a6f7205de65017f847194130e845f2be7", null ]
];