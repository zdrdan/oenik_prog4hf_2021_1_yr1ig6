var namespace_lemez_bolt_1_1_logic =
[
    [ "Tests", "namespace_lemez_bolt_1_1_logic_1_1_tests.html", "namespace_lemez_bolt_1_1_logic_1_1_tests" ],
    [ "BeszerzoLogic", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic.html", "class_lemez_bolt_1_1_logic_1_1_beszerzo_logic" ],
    [ "EladoLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html", "class_lemez_bolt_1_1_logic_1_1_elado_logic" ],
    [ "IBeszerzoLogic", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic.html", "interface_lemez_bolt_1_1_logic_1_1_i_beszerzo_logic" ],
    [ "IEladoLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic" ],
    [ "ITulajdonosLogic", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic" ],
    [ "TulajdonosLogic", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic.html", "class_lemez_bolt_1_1_logic_1_1_tulajdonos_logic" ],
    [ "VeteleiResult", "class_lemez_bolt_1_1_logic_1_1_vetelei_result.html", "class_lemez_bolt_1_1_logic_1_1_vetelei_result" ]
];