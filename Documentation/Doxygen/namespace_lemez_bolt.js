var namespace_lemez_bolt =
[
    [ "Data", "namespace_lemez_bolt_1_1_data.html", "namespace_lemez_bolt_1_1_data" ],
    [ "Logic", "namespace_lemez_bolt_1_1_logic.html", "namespace_lemez_bolt_1_1_logic" ],
    [ "Program", "namespace_lemez_bolt_1_1_program.html", "namespace_lemez_bolt_1_1_program" ],
    [ "Repository", "namespace_lemez_bolt_1_1_repository.html", "namespace_lemez_bolt_1_1_repository" ]
];