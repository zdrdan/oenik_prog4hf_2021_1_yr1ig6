var interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic =
[
    [ "AktivUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html#ae928c6beed76d6eea698f3fc3228b563", null ],
    [ "LegnepszerubbLemez", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html#ad999ea8e7bdacafd9786e4559983f7a8", null ],
    [ "LegnepszerubbLemezAsync", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html#a831a3042e2975a84c54c14cf5d99d37f", null ],
    [ "LemezDeleteLogic", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html#ad9bab8f9e3e10c435478e777f929bf5b", null ],
    [ "TorzsvendegCreateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html#ae71aaf12ea48479f21ecbd936e408205", null ],
    [ "TorzsvendegDeleteLogic", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html#ae593e3029f4854c3d2597267c60a8c6c", null ],
    [ "VasarlasDeleteLogic", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html#a20c4a2c39e09fae6b9881f3ad64a2458", null ],
    [ "VasarlokNevei", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html#ab3441ba92914d15e65ebebf1bfd891cf", null ],
    [ "VasarlokNeveiAsync", "interface_lemez_bolt_1_1_logic_1_1_i_tulajdonos_logic.html#a3583770ae416b6f7edb9259671a82fda", null ]
];