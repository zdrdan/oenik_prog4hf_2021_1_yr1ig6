var class_lemez_bolt_1_1_data_1_1_torzsvendeg =
[
    [ "Torzsvendeg", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#a5bb830be19376b7e65c50859ad1e77a3", null ],
    [ "Equals", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#a5978236bb59311eb7477862d0c87243b", null ],
    [ "GetHashCode", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#adc197c1243bc1b7466783adebdf2eff6", null ],
    [ "Aktiv", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#a5533d4c09116aa6d3926959524a15e4f", null ],
    [ "KedvencStilus", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#a4d5e3018452b9dac6921bd677b91eae4", null ],
    [ "Nev", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#aed60895a211bb42db370a71e7eff32bd", null ],
    [ "Szuletesnap", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#a8ced732a3f7d3ee801a64811b0b8aa20", null ],
    [ "TorzsvendegID", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#a8a428eb119371a3f3dda67a43ed86b60", null ],
    [ "TvEmail", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#a1ba2ad8d0bcd5459b4351a9a9f2756f0", null ],
    [ "Vasarlasok", "class_lemez_bolt_1_1_data_1_1_torzsvendeg.html#a92ccc6c4b03a65d53ee23d1f8902d822", null ]
];