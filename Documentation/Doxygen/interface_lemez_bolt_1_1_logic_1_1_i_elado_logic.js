var interface_lemez_bolt_1_1_logic_1_1_i_elado_logic =
[
    [ "AFAUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a22fed8773b9dec6530d5ff93ed58291b", null ],
    [ "ArUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#aa3dee70678c3a66b444c795cc657244f", null ],
    [ "KedvencStilusUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a40c7320bc94a1a54728d6d10aa3c3ea4", null ],
    [ "LemezIDUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a4f590c03820ed94cbe2186bc6afe9087", null ],
    [ "NevUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#ad19d29271a6ea0f31ef000cb02dbc99f", null ],
    [ "SzuletesnapUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a35eb191155ffdfd48d01c2f84b77fc90", null ],
    [ "TorzsvendegGetOne", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a82b49febabba6309a3e09e66f2394a6d", null ],
    [ "TorzsvendegIDUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a1b4391ea052cbd022a7be7b5eb948a37", null ],
    [ "TorzsvendegRead", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#ac53e6cdbfd77a17e432459d5171996a4", null ],
    [ "TorzsvendegVetelei", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a584b30395889b75bf61d574ae0b34850", null ],
    [ "TorzsvendegVeteleiAsync", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a0bdab45cfe4f0efb6c4dd91baeddca92", null ],
    [ "TvEmailUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a84cf84c9a72db100c2ec77e0caca018b", null ],
    [ "VasarlasCreateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#af03ec826aa4a58a44e631d82d73aa17e", null ],
    [ "VasarlasGetOne", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#aaf5504850348011594be8769bff0367f", null ],
    [ "VasarlasRead", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#a34e4e8cc79f0111edf838aca50a1872e", null ],
    [ "VetelDatumUpdateLogic", "interface_lemez_bolt_1_1_logic_1_1_i_elado_logic.html#ac7831d860acb2bfd58717e07d73a5f11", null ]
];