var class_lemez_bolt_1_1_logic_1_1_elado_logic =
[
    [ "EladoLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a033482e4c91f80cead6a2338c168b58c", null ],
    [ "AFAUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a97c90dc44c89bd6f9412c4667e4717fb", null ],
    [ "ArUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a9599b7458dc7cfd5831f06bccfb16686", null ],
    [ "KedvencStilusUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#af98bd33bbb624cc76634c3e4ef37376a", null ],
    [ "LemezIDUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a34daa0e888866cc7924e3b03fc12fa8f", null ],
    [ "NevUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a17d7e8dc105a742305e79deab618bf9d", null ],
    [ "SzuletesnapUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a6256f8c68b8bde02f7c976959d997247", null ],
    [ "TorzsvendegGetOne", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a247f6948f55c761d7cfb5947483077a8", null ],
    [ "TorzsvendegIDUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a46e58f2f88be941ba87dbcf0bb4336e4", null ],
    [ "TorzsvendegRead", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a2fee356e31025688b37a07d63bc35edf", null ],
    [ "TorzsvendegVetelei", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a1d3cf33bff2437924bfa439b5a644000", null ],
    [ "TorzsvendegVeteleiAsync", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a21761f718dfc2ef6de8abb81804cb0e1", null ],
    [ "TvEmailUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a8fc03a70841b72cefff5c2b31a9de0f1", null ],
    [ "VasarlasCreateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a260cc1163e92cbc5fecc196567471ed4", null ],
    [ "VasarlasGetOne", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a74a8725366080be95da3f37c0c42f1fc", null ],
    [ "VasarlasRead", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a26ef9e20bf9615c2fed4dc86aea4d3b2", null ],
    [ "VetelDatumUpdateLogic", "class_lemez_bolt_1_1_logic_1_1_elado_logic.html#a7181b9478e3e36b1ef40b9aa4c79f4cc", null ]
];