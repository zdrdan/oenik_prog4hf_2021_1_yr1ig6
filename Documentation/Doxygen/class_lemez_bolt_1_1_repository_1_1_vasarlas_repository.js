var class_lemez_bolt_1_1_repository_1_1_vasarlas_repository =
[
    [ "VasarlasRepository", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html#a845a800a14060a463edb6c48a4e7d9bd", null ],
    [ "AFAUpdate", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html#a63ab7fe096db5700afc6293885b580df", null ],
    [ "ArUpdate", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html#af81d3df280d7d55302841e9acf91b48c", null ],
    [ "GetOne", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html#acbffd877eb5348d81793b8a21291ec4d", null ],
    [ "LemezIDUpdate", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html#a32107a0c5933df7b453f7d5ee409d16a", null ],
    [ "TorzsvendegIDUpdate", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html#a1a74253fdee8f35e226cef303f75c4d4", null ],
    [ "VasarlasCreate", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html#ac76d27955990a99d281656671d9c8c27", null ],
    [ "VasarlasDelete", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html#ad4516f9b6881e93e7fe8cde09c73f623", null ],
    [ "VetelDatumUpdate", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html#a7463e41d9f940ac42bad27e732127da2", null ]
];