var namespace_lemez_bolt_1_1_repository =
[
    [ "ILemezRepository", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository.html", "interface_lemez_bolt_1_1_repository_1_1_i_lemez_repository" ],
    [ "IRepository", "interface_lemez_bolt_1_1_repository_1_1_i_repository.html", "interface_lemez_bolt_1_1_repository_1_1_i_repository" ],
    [ "ITorzsvendegRepository", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository.html", "interface_lemez_bolt_1_1_repository_1_1_i_torzsvendeg_repository" ],
    [ "IVasarlasRepository", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository.html", "interface_lemez_bolt_1_1_repository_1_1_i_vasarlas_repository" ],
    [ "LemezRepository", "class_lemez_bolt_1_1_repository_1_1_lemez_repository.html", "class_lemez_bolt_1_1_repository_1_1_lemez_repository" ],
    [ "ParentRepository", "class_lemez_bolt_1_1_repository_1_1_parent_repository.html", "class_lemez_bolt_1_1_repository_1_1_parent_repository" ],
    [ "TorzsvendegRepository", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository.html", "class_lemez_bolt_1_1_repository_1_1_torzsvendeg_repository" ],
    [ "VasarlasRepository", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository.html", "class_lemez_bolt_1_1_repository_1_1_vasarlas_repository" ]
];